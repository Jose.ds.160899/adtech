<div class="modal fade" id="edit_<?php echo $clientes[0]; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                <h3 class="panel-title"><span class="glyphicon glyphicon-list" aria-hidden="true"></span> Editar Cliente</h3>
	            </div>
	        </div>
	        <form class="form-horizontal" method="POST" action="guardar_cliente.php">
	            <div class="modal-body">					
	            	<input type="hidden" name="client_id" value="<?php echo $clientes[0] ?>">
					<input type="hidden" name="actualiza" value="1">
					<div class="container-fluid">
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label text-primary" style="position:relative; top:7px;">Nombre:</label>
							</div>
							<div class="col-sm-10">
								<input type="text" maxlength="50" class="form-control" id="nombre" name="nombre" placeholder="Ingresa nombres" onkeypress="return soloLetras(event)" value="<?php echo $clientes[2];?>" required autofocus="">
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label text-primary" style="position:relative; top:7px;">Tipo Doc:</label>
							</div>
							<div class="col-sm-10"> 
						      <select class="form-control" id="tipo" name="tipo" onchange="mostrarValor(this.value)">
						      	<option value disabled selected>Seleccione</option>
						      	<option <?php if ($clientes[3]=='DNI') { echo 'selected';} ?> value="<?php echo $clientes[3] ?>"> <?php echo $clientes[3] ?> </option>
						      	<option <?php if ($clientes[3]=='RUC') { echo 'selected';} ?> value="<?php echo $clientes[3] ?>">RUC</option>
						      </select>
						    </div>
					  	</div>
					  	<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label text-primary" style="position:relative; top:7px;">Nro Doc:</label>
							</div>
							<div class="col-sm-10"> 
						      <input type="text" name="doc" class="form-control" onkeypress="if (event.keyCode < 48 || event.keyCode > 57) event.returnValue = false;" id="doc"  maxlength=8 placeholder="Ingresa número de documento" value="<?php echo $clientes[4] ?>" required="">
						    </div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label text-primary" style="position:relative; top:7px;">Dirección:</label>
							</div>
							<div class="col-sm-10">
					      		<input type="text" maxlength="50" class="form-control" id="dir" name="dir" placeholder="Ingresa dirección" onkeypress="return soloLetras(event)" value="<?php echo $clientes[5] ?>" required="" autofocus="">
					    	</div>
					  	</div>
					  	<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label text-primary" style="position:relative; top:7px;">Teléfono:</label>
							</div>
							<div class="col-sm-10">
					      		<input type="text" maxlength="9" class="form-control" id="tel" name="tel" required="" placeholder="Ingresa número de teléfono" autofocus="" value="<?php echo $clientes[6]?>">
					    	</div>
					  	</div>
					  	<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label text-primary" style="position:relative; top:7px;">Email:</label>
							</div>
							<div class="col-sm-10">
					      		<input type="email" maxlength="50" class="form-control" id="mail" name="mail" placeholder="Ingresa email" required="" autofocus="" value="<?php echo $clientes[7] ?>">
					    	</div>
					  	</div>
					  	<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label text-primary" style="position:relative; top:7px;">Lim.Crédito:</label>
							</div>
							<div class="col-sm-10">
					      		<input type="number" min="0.00" step="any" class="form-control" id="limit_cred" name="limit_cred" placeholder="Ingresa el límite de Crédito" onkeypress="return filterFloat(event,this);" required="" value="<?php echo $clientes[8] ?>">
					    	</div>
					  	</div>
					  	<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label text-primary" style="position:relative; top:7px;">Cant. días:</label>
							</div>
							<div class="col-sm-10">
								<select class="form-control" name="cant_day" id="cant_day">
					      			<option disabled selected>Seleccione</option>
					      			<option <?php if ($clientes[9]=="15 DAY") { echo "selected";} ?> value="15 DAY">15 días</option>
					      			<option <?php if ($clientes[9]=="30 DAY") { echo "selected";} ?> value="30 DAY">30 días</option>
					      			<option <?php if ($clientes[9]=="45 DAY") { echo "selected";} ?> value="45 DAY">45 días</option>
					      			<option <?php if ($clientes[9]=="60 DAY") { echo "selected";} ?> value="60 DAY">60 días</option>
					      			<option <?php if ($clientes[9]=="90 DAY") { echo "selected";} ?> value="90 DAY">90 días</option>
					      		</select>
					    	</div>
					  	</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label text-primary" style="position:relative; top:7px;">Estado:</label>
							</div>
							<div class="col-sm-10">
								<label class="switch">
									<input type="checkbox" class="form-control" name="estado" value="1" <?php if ($clientes[10]==1) { echo "checked";} ?>>
									<span class="slider round"></span>
								</label>	
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
	                <center><button type="button" class="btn btn-white btn-warning btn-sm btn-round" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancelar</button>
	                <button type="submit" name="editar" class="btn btn-white btn-success btn-sm btn-round"><span class="glyphicon glyphicon-check"></span> Actualizar</button></center>
	            </div>
			</form>
		</div>
	</div>
</div>
<!-- Delete -->
<div class="modal fade" id="delete_<?php echo $clientes[0]; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="panel panel-warning">
				<div class="panel-heading">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                <h3 class="panel-title"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Eliminar Cliente</h3>
	            </div>
	        </div>
            <div class="modal-body">    
                <h5 class="text-center text-danger">Esta seguro de borrar al Cliente:</h5>
                <h6 class="text-center text-primary"><?php echo $clientes[2]; ?></h6>
            </div>
            <div class="modal-footer center">
                <button type="button" class="btn btn-white btn-warning btn-sm btn-round" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancelar</button>
                <a href="guardar_cliente.php?id=<?php echo $clientes[0]; ?>" class="text-success"><button type="submit" name="editar" class="btn btn-white btn-success btn-sm btn-round"><span class="glyphicon glyphicon-trash"></span> Eliminar</button></a></center>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
	function validaNumericos(event) {
    if(event.charCode >= 48 && event.charCode <= 57){
      return true;
     }
     return false;        
}
var mostrarValor = function(x){
    if (x=='DNI') {
        document.getElementById('doc').maxLength=8;
        document.getElementById('doc').readOnly=false;
        document.getElementById('doc').focus();
    }else{
    	document.getElementById('doc').maxLength=11;
    	document.getElementById('doc').readOnly=false;
        document.getElementById('doc').focus();
    }
}
 function soloLetras(e){
       key = e.keyCode || e.which;
       tecla = String.fromCharCode(key).toLowerCase();
       letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
       especiales = "8-37-39-46";

       tecla_especial = false
       for(var i in especiales){
            if(key == especiales[i]){
                tecla_especial = true;
                break;
            }
        }

        if(letras.indexOf(tecla)==-1 && !tecla_especial){
            return false;
        }
    }
</script>
<script type="text/javascript">
	function filterFloat(evt,input){
    // Backspace = 8, Enter = 13, ‘0′ = 48, ‘9′ = 57, ‘.’ = 46, ‘-’ = 43
    var key = window.Event ? evt.which : evt.keyCode;
    var chark = String.fromCharCode(key);
    var tempValue = input.value+chark;
    if(key >= 48 && key <= 57){
        if(filter(tempValue)=== false){
            return false;
        }else{
            return true;
        }
    }else{
          if(key == 8 || key == 13 || key == 0) {
              return true;
          }else if(key == 46){
                if(filter(tempValue)=== false){
                    return false;
                }else{
                    return true;
                }
          }else{
              return false;
          }
    }
}
function filter(__val__){
    var preg = /^([0-9]+\.?[0-9]{0,2})$/;
    if(preg.test(__val__) === true){
        return true;
    }else{
       return false;
    }

}
$(function(){
    $(".validar").keydown(function(event){
        //alert(event.keyCode);
        if((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105) && event.keyCode !==190  && event.keyCode !==110 && event.keyCode !==8 && event.keyCode !==9  ){
            return false;
        }
    });
});
</script>