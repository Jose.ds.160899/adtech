<?php
	$rs_proveedor=mysqli_query($conex,"SELECT*FROM proveedor");
	$rs_tipo_doc=mysqli_query($conex,"SELECT * FROM correlativo WHERE Tipo_doc='Comprobante'");
	$fecha=date('Y-m-d');
 ?>
<div class="modal fade" id="modal-compra">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="panel panel-success">
				<div class="panel-heading">
	    			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        		<h3 class="panel-title"><span class="glyphicon glyphicon-list" aria-hidden="true"></span> Pagos de Proveedor</h3>
	    		</div>
			</div>
			<form class="form-horizontal" method="POST" action="guardar_compra.php">
				<div class="modal-body">
					<div class="container-fluid">
						<div class="form-group">
					    	<label class="control-label col-sm-3 text-primary label-round">Proveedor:</label>
					    	<div class="col-sm-9">
							    <select class="chosen-select form-control" name="id_prov">
							    	<option disabled selected>Seleccione</option><?php
							    	while ($proveedor=mysqli_fetch_row($rs_proveedor)) { ?>
							    	<option value="<?php echo $proveedor[0];?>"><?php echo $proveedor[2];?></option><?php }	?>
							   	</select>
					    	</div>
					  	</div>
					  	<div class="form-group">
					  		<label  class="control-label col-sm-3 text-primary">Forma Pago:</label>
					  		<div class="col-sm-9">
					  			<select class="chosen-select form-control" name="f_pago">
					  				<option disabled selected>Seleccione</option>
						      		<option value="EFE">EFECTIVO</option>
						      		<option value="DBBVA">DEPOSITO BBVA</option>
						      		<option value="DBCP">DEPOSITO BCP</option>
						      		<option value="DSCO">DEPOSITO SCOTIABANK</option>
						      		<option value="PLIN">PLIN</option>
						      		<option value="YAPE">YAPE</option>
						      	</select>
					    	</div>
					  	</div>
					  	<div class="form-group">
					  		<label  class="control-label col-sm-3 text-primary">Monto:</label>
					  		<div class="col-sm-9">
					      		<input type="number" name="cant" min="1" step="0.01" class="form-control" placeholder=" Ingresa la cantidad" required>
					    	</div>
					  	</div>
					  	<div class="form-group">
					    	<label class="control-label col-sm-3 text-primary" for="">Fecha:</label>
					    	<div class="col-sm-9">
					      		<input type="date" name="fecha_pago" class="form-control" value="<?php echo $fecha ?>" required="">
					    	</div>
					  	</div>
					  	<div class="form-group">
					    	<label class="control-label col-sm-3 text-primary" for="">Tipo Doc.:</label>
					    	<div class="col-sm-9">
					    		<select class="chosen-select form-control" id="tipo" name="tipo" onchange="mostrarValor(this.value)">
					    			<option disabled selected>Seleccione</option><?php
	        						while ($tipo_doc=mysqli_fetch_row($rs_tipo_doc)) { ?>
	            						<option value="<?php echo $tipo_doc[1];?>"><?php echo $tipo_doc[1];?></option><?php
	            					} ?>
					    		</select>
					    	</div>
					  	</div>
					  	<div class="form-group">
					    	<label class="control-label col-sm-3 text-primary" for="">N° Doc.:</label>
					    	<div class="col-sm-9">
					      		<input type="text" name="dcomp" class="form-control" required="">
					    	</div>
					  	</div>
					</div>
				</div>
				<div class="modal-footer">
					<center>
						<button type="button" class="btn btn-white btn-warning btn-sm btn-round" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancelar</button>
						<button type="submit" value="add" class="btn btn-white btn-success btn-sm btn-round"><span class="glyphicon glyphicon-check"></span> Registrar</button>
					</center>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
	function filterFloat(evt,input){
    // Backspace = 8, Enter = 13, ‘0′ = 48, ‘9′ = 57, ‘.’ = 46, ‘-’ = 43
    var key = window.Event ? evt.which : evt.keyCode;
    var chark = String.fromCharCode(key);
    var tempValue = input.value+chark;
    if(key >= 48 && key <= 57){
        if(filter(tempValue)=== false){
            return false;
        }else{
            return true;
        }
    }else{
          if(key == 8 || key == 13 || key == 0) {
              return true;
          }else if(key == 46){
                if(filter(tempValue)=== false){
                    return false;
                }else{
                    return true;
                }
          }else{
              return false;
          }
    }
}
function filter(__val__){
    var preg = /^([0-9]+\.?[0-9]{0,2})$/;
    if(preg.test(__val__) === true){
        return true;
    }else{
       return false;
    }

}
</script>
