<?php
/*session_start();
require_once('conexion.php');*/
$tipo_prod=mysqli_query($conex,"SELECT * FROM tipo_articulo");
$tipo_prov=mysqli_query($conex,"SELECT * FROM proveedor WHERE Estado=1");
$cat=mysqli_query($conex,"SELECT * FROM categorias WHERE estado=1");
 ?>
<div class="modal fade" id="modal-item" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="panel panel-success">
				<div class="panel-heading">
	    			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        		<h3 class="panel-title"><span class="glyphicon glyphicon-list" aria-hidden="true"></span> Ingreso de Productos</h3>
	    		</div>
			</div>
			<form class="form-horizontal" method="POST" action="guardar_articulo.php">
				<div class="modal-body">
					<input type="hidden" id="item-id">
					<div class="container-fluid">
					  	<div class="form-group">
					  		<label class="control-label col-sm-3 text-primary label-round" for="">Producto:</label>
						    <div class="col-sm-9">
						      <input type="text" maxlength="50" class="form-control" id="item-name" name="item-name" placeholder="Ingresa nombre producto" required="" autofocus="">
						    </div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-3 text-primary" for="">Precio Compra:</label>
						    <div class="col-sm-9">
						      <input type="number" min="0.1" step="any" class="form-control" id="precioc" name="precioc" placeholder=" Precio Compra" onkeypress="return filterFloat(event,this);" required="">
						    </div>
					  	</div>
						<!-- <div class="form-group">
						    <label class="control-label col-sm-3 text-primary" for="">Precio Compra:</label>
						    <div class="col-sm-9">
						      <input type="text" maxlength="50" class="form-control validar" id="precioc" name="precioc" placeholder="Ingresa Precio de Compra" required="" autofocus="" onkeypress="return filterFloat(event,this);">
						    </div>
						</div> -->
						<div class="form-group">
							<label class="control-label col-sm-3 text-primary" for="">Precio Venta:</label>
						    <div class="col-sm-9">
						      <input type="number" min="0.1" step="any" class="form-control" id="preciov" name="preciov" placeholder=" Precio de Venta" onkeypress="return filterFloat(event,this);" required="">
						    </div>
					  	</div>
						<div class="form-group">
						    <label class="control-label col-sm-3 text-primary"  for="">Categoria:</label>
						    <div class="col-sm-9">
						      <select class="chosen-select form-control" name="cat" style="width: 100%">
						      	<option selected disabled>Seleccionar</option><?php
	                				while ($categoria=mysqli_fetch_row($cat)) { ?>
	                    			<option value="<?php echo $categoria[0];?>"><?php echo $categoria[1];?></option><?php
	                				} ?>
						      </select>
						    </div>
						</div>
						<div class="form-group">
						    <label class="control-label col-sm-3 text-primary" for="">Proveedor:</label>
						    <div class="col-sm-9">
						      <select class="chosen-select form-control" name="fab" style="width: 100%">
						      	<option selected disabled>Seleccionar</option><?php
	                				while ($prov=mysqli_fetch_row($tipo_prov)) { ?>
	                    			<option value="<?php echo $prov[0];?>"><?php echo $prov[2];?></option><?php
	                				} ?>
						      </select>
							</div>
						</div>
						<div class="form-group">
						    <label class="control-label col-sm-3 text-primary" for="">UM:</label>
						    <div class="col-sm-9">
						      <select class="chosen-select form-control" name="tipo_art" style="width: 100%">
						      	<option selected disabled>Seleccionar</option><?php
	                				while ($tipo=mysqli_fetch_row($tipo_prod)) { ?>
	                    			<option value="<?php echo $tipo[0];?>"><?php echo $tipo[1];?></option><?php
	                				} ?>
						      </select>
							</div>
						</div>
						<!--<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
						      <button type="submit" id="submit-item" value="add" class="btn btn-success btn-sm">Guardar datos
						      <span class="glyphicon glyphicon-save" aria-hidden="true"></span>
						      </button>
						    </div>
						</div>-->
					</div>
				</div>
				<div class="modal-footer">
					<center>
						<button type="button" class="btn btn-white btn-warning btn-sm btn-round" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancelar</button>
						<button type="submit" value="add" class="btn btn-white btn-success btn-sm btn-round"><span class="glyphicon glyphicon-check"></span> Registrar</button>
					</center>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
	function filterFloat(evt,input){
    // Backspace = 8, Enter = 13, ‘0′ = 48, ‘9′ = 57, ‘.’ = 46, ‘-’ = 43
    var key = window.Event ? evt.which : evt.keyCode;
    var chark = String.fromCharCode(key);
    var tempValue = input.value+chark;
    if(key >= 48 && key <= 57){
        if(filter(tempValue)=== false){
            return false;
        }else{
            return true;
        }
    }else{
          if(key == 8 || key == 13 || key == 0) {
              return true;
          }else if(key == 46){
                if(filter(tempValue)=== false){
                    return false;
                }else{
                    return true;
                }
          }else{
              return false;
          }
    }
}
function filter(__val__){
    var preg = /^([0-9]+\.?[0-9]{0,2})$/;
    if(preg.test(__val__) === true){
        return true;
    }else{
       return false;
    }

}
$(function(){
    $(".validar").keydown(function(event){
        //alert(event.keyCode);
        if((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105) && event.keyCode !==190  && event.keyCode !==110 && event.keyCode !==8 && event.keyCode !==9  ){
            return false;
        }
    });
});
</script>
