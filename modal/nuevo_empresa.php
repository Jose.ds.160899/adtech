<div class="modal fade" id="modal-empresa" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="panel panel-success">
				<div class="panel-heading">
	    			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        		<h3 class="panel-title"><span class="glyphicon glyphicon-list" aria-hidden="true"></span> Datos de la Empresa</h3>
	    		</div>
			</div>
			<form class="form-horizontal" enctype="multipart/form-data" method="POST" action="guardar_empresa.php">
				<div class="modal-body">
					<input type="hidden" id="user-id">
				  	<div class="form-group">
				  		<label class="control-label col-sm-3 text-primary label-round" for="">R. Social:</label>
					    <div class="col-sm-9">
					      <input type="text" maxlength="50" class="form-control" id="user-name" name="user-name" placeholder="Ingresar Nombre" required="" autofocus="">
					    </div>
					</div>
					<div class="form-group">
					    <label class="control-label col-sm-3 text-primary" for="">Tipo Doc.:</label>
					    <div class="col-sm-9"> 
					      <select id="tipo" name="tipo" onchange="mostrarValor(this.value)" style="width: 100%">
					      	<option value disabled selected>Seleccione</option>
					      	<option value="DNI">DNI</option>
					      	<option value="RUC">RUC</option>
					      </select>
					    </div>
					  </div>
					  <div class="form-group">
					    <label class="control-label col-sm-3 text-primary" for="">Núm Doc:</label>
					    <div class="col-sm-9"> 
					      <input type="text" name="doc" class="form-control" onkeypress="if (event.keyCode < 48 || event.keyCode > 57) event.returnValue = false;" id="doc"  maxlength=8 placeholder="Ingresa número de documento" required="">
					    </div>
					  </div>
					<div class="form-group">
					    <label class="control-label col-sm-3 text-primary" for="">Dirección:</label>
					    <div class="col-sm-9">
					      <input type="text" maxlength="50" name="dir" class="form-control" placeholder="Ingresa dirección" required="" autofocus="">
					    </div>
					</div>
					<div class="form-group">
					    <label class="control-label col-sm-3 text-primary" for="">Telefono:</label>
					    <div class="col-sm-9">
					      <input type="tel" maxlength="9" class="form-control validar" id="cel" name="cel" placeholder="Ingresa número" required="" autofocus="" onkeypress="return filterFloat(event,this);">
					    </div>
					</div>
					<div class="form-group">
					    <label class="control-label col-sm-3 text-primary" for="">Email:</label>
					    <div class="col-sm-9">
					      <input type="email" maxlength="50" class="form-control" id="mail" name="mail" placeholder="Ingresa email" required="" autofocus="">
					    </div>
					</div>
					<div class="form-group">
					    <label class="control-label col-sm-3 text-primary" for="">Logo:</label>
					    <div class="col-sm-9">
					    	<label class="ace-file-input">
								<input type="file" id="logo" name="logo" />
								<span class="ace-file-container" data-title="seleccionar"></span>
								<span class="ace-file-name" data-title="Sin adjunto">...</span>
							</label>
						</div>
					</div>	
				</div>
				<div class="modal-footer">
					<center>
						<button type="button" class="btn btn-white btn-warning btn-sm btn-round" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancelar</button>
						<button type="submit" value="add" class="btn btn-white btn-success btn-sm btn-round"><span class="glyphicon glyphicon-check"></span> Registrar</button>
					</center>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
	function validaNumericos(event) {
    if(event.charCode >= 48 && event.charCode <= 57){
      return true;
     }
     return false;        
}
var mostrarValor = function(x){
    if (x=='DNI') {
        document.getElementById('doc').maxLength=8;
        document.getElementById('doc').readOnly=false;
        document.getElementById('doc').focus();
    }else{
    	document.getElementById('doc').maxLength=11;
    	document.getElementById('doc').readOnly=false;
        document.getElementById('doc').focus();
    }
}
 function soloLetras(e){
       key = e.keyCode || e.which;
       tecla = String.fromCharCode(key).toLowerCase();
       letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
       especiales = "8-37-39-46";

       tecla_especial = false
       for(var i in especiales){
            if(key == especiales[i]){
                tecla_especial = true;
                break;
            }
        }

        if(letras.indexOf(tecla)==-1 && !tecla_especial){
            return false;
        }
    }
</script>
<script type="text/javascript">
	function filterFloat(evt,input){
    // Backspace = 8, Enter = 13, ‘0′ = 48, ‘9′ = 57, ‘.’ = 46, ‘-’ = 43
    var key = window.Event ? evt.which : evt.keyCode;
    var chark = String.fromCharCode(key);
    var tempValue = input.value+chark;
    if(key >= 48 && key <= 57){
        if(filter(tempValue)=== false){
            return false;
        }else{
            return true;
        }
    }else{
          if(key == 8 || key == 13 || key == 0) {
              return true;
          }else if(key == 46){
                if(filter(tempValue)=== false){
                    return false;
                }else{
                    return true;
                }
          }else{
              return false;
          }
    }
}
function filter(__val__){
    var preg = /^([0-9]+\.?[0-9]{0,2})$/;
    if(preg.test(__val__) === true){
        return true;
    }else{
       return false;
    }

}
$(function(){
    $(".validar").keydown(function(event){
        //alert(event.keyCode);
        if((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105) && event.keyCode !==190  && event.keyCode !==110 && event.keyCode !==8 && event.keyCode !==9  ){
            return false;
        }
    });
});
</script>