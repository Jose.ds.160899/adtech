<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');
	require_once('conexion.php');
	$rs_stock=mysqli_query($conex,"SELECT * FROM articulo");
	$fecha=date('Y-m-d');
 ?>
<div class="modal fade" id="modal-stock">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="panel panel-success">
				<div class="panel-heading">
	    			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        		<h3 class="panel-title"><span class="glyphicon glyphicon-list" aria-hidden="true"></span> Nuevo Stock</h3>
	    		</div>
			</div>
			<form class="form-horizontal" method="POST" action="guardar_stock.php">
				<div class="modal-body">
					<div class="container-fluid">
						<div class="form-group">
					    	<label class="control-label col-sm-3 text-primary label-round">Producto:</label>
					    	<div class="col-sm-9">
						    	<select class="chosen-select form-control" name="id_art">
									<option disabled selected>Seleccione</option><?php
						    		while ($stock=mysqli_fetch_row($rs_stock)) { ?>
						    		<option value="<?php echo $stock[0];?>"><?php echo $stock[4];?></option><?php }	?>
						   		</select>
					    	</div>
					  	</div>
					  	<div class="form-group">
					  		<label  class="control-label col-sm-3 text-primary">Cantidad:</label>
					  		<div class="col-sm-9">
					      		<input type="number" name="cant" min="1" class="form-control" placeholder=" Ingresa la cantidad" required="" onkeypress="return filterFloat(event,this);">
					    	</div>
					  	</div>
					  	<div class="form-group">
					    	<label class="control-label col-sm-3 text-primary" for="">Comprado:</label>
					    	<div class="col-sm-9">
					      		<input type="date" name="fcomp" class="form-control" value="<?php echo $fecha ?>" required="">
					    	</div>
					  	</div>
					  	<div class="form-group">
					    	<label class="control-label col-sm-3 text-primary" for="">Vence:</label>
				    		<div class="col-sm-9">
					      		<input type="date" name="fvenc" class="form-control" value="<?php echo $fecha ?>" min="<?php echo $fecha ?>" required="">
					    	</div>
					  	</div>
				  	</div>
				</div>
				<div class="modal-footer">
					<center>
						<button type="button" class="btn btn-white btn-warning btn-sm btn-round" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancelar</button>
						<button type="submit" value="add" class="btn btn-white btn-success btn-sm btn-round"><span class="glyphicon glyphicon-check"></span> Registrar</button>
					</center>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
	function filterFloat(evt,input){
    // Backspace = 8, Enter = 13, ‘0′ = 48, ‘9′ = 57, ‘.’ = 46, ‘-’ = 43
    var key = window.Event ? evt.which : evt.keyCode;
    var chark = String.fromCharCode(key);
    var tempValue = input.value+chark;
    if(key >= 48 && key <= 57){
        if(filter(tempValue)=== false){
            return false;
        }else{
            return true;
        }
    }else{
          if(key == 8 || key == 13 || key == 0) {
              return true;
          }else if(key == 46){
                if(filter(tempValue)=== false){
                    return false;
                }else{
                    return true;
                }
          }else{
              return false;
          }
    }
}
function filter(__val__){
    var preg = /^([0-9]+\.?[0-9]{0,2})$/;
    if(preg.test(__val__) === true){
        return true;
    }else{
       return false;
    }

}
</script>
