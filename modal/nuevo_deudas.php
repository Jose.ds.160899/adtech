<?php 
	$rs_deudas=mysqli_query($conex,"SELECT * FROM deudas WHERE Id_client=$clientes[0]");
?>
<div class="modal fade" id="deuda_<?php echo $clientes[0]; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="panel panel-success">
				<div class="panel-heading">
	    			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        		<h3 class="panel-title"><span class="glyphicon glyphicon-list" aria-hidden="true"></span> Reporte de deudas</h3>
	    		</div>
			</div>
				<div class="modal-body">
					<div class="container-fluid">
						
                            <thead>
                                <tr class="info">
                                    <th>ID</th>
                                    <th>FECHA</th>
                                    <th>HORA</th>
                                    <th>TOTAL</th>
                                </tr>
                            </thead>
                            <tbody><?php $x=1;
                            while ($deudas=mysqli_fetch_row($rs_deudas)) { ?>
                                <tr>
                                    <td><?php echo $x?></td>
                                    <td><?php echo $deudas[3]?></td>
                                    <td><?php echo $deudas[4]?></td>
                                    <td><?php echo $deudas[6]?></td>
                                </tr><?php $x++;
                            }?>
                            </tbody>
                        
					</div>
				</div>
		</div>
	</div>
</div>