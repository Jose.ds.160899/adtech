<div class="modal fade" id="modal-tipo_documento">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="panel panel-success">
				<div class="panel-heading">
	    			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        		<h3 class="panel-title"><span class="glyphicon glyphicon-list" aria-hidden="true"></span> Registrar Documento</h3>
	    		</div>
			</div>
			<form class="form-horizontal" method="POST" action="guardar_tipo_comprobante.php">
				<div class="modal-body">
					<div class="container-fluid">
						<div class="form-group">
							<label  class="control-label col-sm-3 text-primary">Nombre Doc:</label>
						  	<div class="col-sm-9">
						      <input type="text" name="nombre" class="form-control" placeholder=" Ingresa la Categoría" required>
						    </div>
						</div>
					  	<div class="form-group">
					    	<label class="control-label col-sm-3 text-primary" for="">Operación:</label>
					    	<div class="col-sm-9">
					    		<select class="form-control" name="tipo_op">
					    			<option disabled selected>Seleccione</option>
					    			<option value="Persona">Persona</option>
					    			<option value="Comprobante">Comprobante</option>
					    		</select>
					    	</div>
					  	</div>
					  	<div class="form-group">
							<label class="control-label col-sm-3 text-primary" for="">Estado:</label>
							<label class="switch">
								<input type="checkbox" class="form-control" name="estado" value="1" checked>
								<span class="slider round"></span>
							</label>	
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<center><button type="submit" value="add" class="btn btn-white btn-success btn-sm btn-round">Guardar <i class="ace-icon fa fa-save green"></i></button></center>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
	function filterFloat(evt,input){
    // Backspace = 8, Enter = 13, ‘0′ = 48, ‘9′ = 57, ‘.’ = 46, ‘-’ = 43
    var key = window.Event ? evt.which : evt.keyCode;
    var chark = String.fromCharCode(key);
    var tempValue = input.value+chark;
    if(key >= 48 && key <= 57){
        if(filter(tempValue)=== false){
            return false;
        }else{
            return true;
        }
    }else{
          if(key == 8 || key == 13 || key == 0) {
              return true;
          }else if(key == 46){
                if(filter(tempValue)=== false){
                    return false;
                }else{
                    return true;
                }
          }else{
              return false;
          }
    }
}
function filter(__val__){
    var preg = /^([0-9]+\.?[0-9]{0,2})$/;
    if(preg.test(__val__) === true){
        return true;
    }else{
       return false;
    }

}
</script>
