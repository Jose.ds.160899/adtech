<?php
$usuario=mysqli_fetch_row(mysqli_query($conex,"SELECT*FROM usuario WHERE Num_doc='$nomb'"));
$rs_inventario=mysqli_query($conex,"SELECT a.Nombre,a.Composicion,a.p_compra,c.Nom_tipo_art,a.Fabricante,a.Precio,b.Stock,b.Fecha_Venc,b.id_stock,a.id_art FROM articulo a INNER JOIN stock b ON a.Id_art = b.Id_art INNER JOIN tipo_articulo c ON a.Id_tipo_art=c.Id_tipo_art AND b.stock>0");
?>
<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="modal-venta">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
			<div class="col-md-12">
	  		<div class="panel panel-success">
	    		<div class="panel-heading">
	    			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        		<h3 class="panel-title"><span class="glyphicon glyphicon-list" aria-hidden="true"></span> Consulta Productos</h3>
	    		</div>
	    		<div class="table-responsive">
	      			<table id="Myorder" class="table table-striped table-bordered table-hover" style="width:100% ">
	        			<thead>
	                      <tr class="info">
	                        <th>Producto</th>
	                        <th>Tipo</th>
	                        <th>Proveedor</th>
	                        <th>Precio</th>
	                        <th>Stock</th>
	                        <th>Vence</th>
	                        <th>Cant</th>
	                        <th>Cargar</th>
	                      </tr>
	                    </thead>
	                    <tbody><?php $i=1;
	                      while ($venta=mysqli_fetch_row($rs_inventario)) {
	                        $prov=mysqli_fetch_row(mysqli_query($conex,"SELECT * FROM proveedor WHERE Id_prov='$venta[4]'"));
	                        $date = date_create($venta[7]);
	                        ?>
	                      <tr class="p-0">
	                        <td><?php echo $venta[0] ?></td>
	                        <td style="font-size: 9px"><?php echo $venta[1].' '.$venta[2].' / '.$venta[3]; ?></td>
	                        <td><?php echo $prov[2] ?></td>
	                        <td><?php echo $venta[5] ?></td>
	                        <td align="center" style="color: green"><span class="badge badge-warning badge-stock-<?php echo $i?>"><?php echo $venta[6] ?></span></td>
	                        <td><?php echo date_format($date,'M-Y') ?></td>
	                        <!-- <form method="POST" action="guardar_carrito.php"> -->
	                          <td align="center" width="8%">
                                <input style="width: 50%;text-align: center; font-size: 12px;" type="number" min="0" id="cant<?php echo $i?>" name="cant<?php echo $i?>" value="1">
                            </td>
      				              <input type="hidden" id="id_art<?php echo $i?>" name="id_art<?php echo $i?>" value="<?php echo $venta[9];?>">
      				              <input type="hidden" id="id_stock<?php echo $i?>" name="id_stock<?php echo $i?>" value="<?php echo $venta[8];?>">
      				              <input type="hidden" id="id_user<?php echo $i?>" name="id_user<?php echo $i?>" value="<?php echo $usuario[0];?>">
      				              <input type="hidden" id="precio_art<?php echo $i?>" name="precio_art<?php echo $i?>" value="<?php echo $venta[5];?>">
      				              <input type="hidden" id="stock<?php echo $i?>" name="stock<?php echo $i?>" value="<?php echo $venta[6];?>">
				              <td align="center">
                        <button type="submit" class="btn btn-white btn-success btn-xs btn-round btn-agregar-producto-modal" onClick='agregar(<?php echo $i?>)'><span class="glyphicon glyphicon-shopping-cart" title="Agregar Producto" aria-hidden="true"></span></button></td>
	                          <!-- </form> -->
	                      </tr>
	                      <?php $i++; } ?>
	                    </tbody>
	                </table>
	            </div>
	        </div>
	    </div><!-- /.col -->
    </div>
  </div>
</div>