<div class="modal fade" id="edit_<?php echo $correlativo[0]; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                <h3 class="panel-title"><span class="glyphicon glyphicon-list" aria-hidden="true"></span> Editar Documentos</h3>
	            </div>
	        </div>
	        <form class="form-horizontal" method="POST" action="guardar_tipo_comprobante.php">
	            <div class="modal-body">
	            	<input type="hidden" name="id_corr" value="<?php echo $correlativo[0]?>">
	            	<input type="hidden" name="actualiza" value="1">
	            	<div class="container-fluid">
	            		<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label text-primary" style="position:relative; top:7px;">Documento:</label>
							</div>
							<div class="col-sm-10">
								<input type="text" name="nombre" class="form-control" value="<?php echo $correlativo[1] ?>" required>
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label text-primary" style="position:relative; top:7px;">Operación:</label>
							</div>
							<div class="col-sm-10">
								<select class="form-control" name="tipo_op">
						    		<option disabled selected>Seleccione</option>
						    		<option <?php if ($correlativo[2]=='Persona'){echo 'selected';} ?> value="Persona">Persona</option>
						    		<option <?php if ($correlativo[2]=='Comprobante'){echo 'selected';} ?> value="Comprobante">Comprobante</option>
						    	</select>
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label text-primary" style="position:relative; top:7px;">Estado:</label>
							</div>
							<div class="col-sm-10">
								<label class="switch">
									<input type="checkbox" class="form-control" name="estado" value="1" <?php if ($correlativo[3]==1) { echo "checked";} ?>><span class="slider round"></span>
								</label>	
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
	                <center><button type="button" class="btn btn-white btn-warning btn-sm btn-round" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancelar</button>
	                <button type="submit" name="editar" class="btn btn-white btn-success btn-sm btn-round"><span class="glyphicon glyphicon-check"></span> Actualizar</button></center>
	            </div>
			</form>
		</div>
	</div>
</div>
<!-- Delete -->
<div class="modal fade" id="delete_<?php echo $correlativo[0]; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="panel panel-warning">
				<div class="panel-heading">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                <h3 class="panel-title"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Eliminar Comprobante</h3>
	            </div>
	        </div>
            <div class="modal-body">    
                <h5 class="text-center text-danger">Esta seguro de borrar el documento:</h5>
                <h6 class="text-center text-primary"><?php echo $correlativo[1]; ?></h6>
            </div>
            <div class="modal-footer center">
                <button type="button" class="btn btn-white btn-warning btn-sm btn-round" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancelar</button>
                <a href="guardar_tipo_comprobante.php?id=<?php echo $correlativo[0]; ?>" class="text-success"><button type="submit" name="editar" class="btn btn-white btn-success btn-sm btn-round"><span class="glyphicon glyphicon-trash"></span> Eliminar</button></a></center>
            </div>
        </div>
    </div>
</div>	
<script type="text/javascript">
	function filterFloat(evt,input){
    // Backspace = 8, Enter = 13, ‘0′ = 48, ‘9′ = 57, ‘.’ = 46, ‘-’ = 43
    var key = window.Event ? evt.which : evt.keyCode;
    var chark = String.fromCharCode(key);
    var tempValue = input.value+chark;
    if(key >= 48 && key <= 57){
        if(filter(tempValue)=== false){
            return false;
        }else{
            return true;
        }
    }else{
          if(key == 8 || key == 13 || key == 0) {
              return true;
          }else if(key == 46){
                if(filter(tempValue)=== false){
                    return false;
                }else{
                    return true;
                }
          }else{
              return false;
          }
    }
}
function filter(__val__){
    var preg = /^([0-9]+\.?[0-9]{0,2})$/;
    if(preg.test(__val__) === true){
        return true;
    }else{
       return false;
    }

}
</script>
