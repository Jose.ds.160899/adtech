<div class="modal fade" id="modal-proveedor" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="panel panel-success">
				<div class="panel-heading">
	    			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        		<h3 class="panel-title"><span class="glyphicon glyphicon-list" aria-hidden="true"></span> Nuevo Proveedor</h3>
	    		</div>
			</div>
			<form class="form-horizontal" method="POST" action="guardar_proveedor.php">
				<div class="modal-body">
					<input type="hidden" id="client-id">
					<div class="container-fluid">
						<div class="form-group">
					    	<label class="control-label col-sm-3 text-primary" for="">R. Social:</label>
					    	<div class="col-sm-9">
					      		<input type="text" maxlength="100" class="form-control" id="r_social" name="r_social" placeholder="Ingresa Razón Social" onkeypress="return soloLetras(event)" required autofocus="">
					    	</div>
					  	</div>
					  	<div class="form-group">
					    	<label class="control-label col-sm-3 text-primary" for="">RUC:</label>
					    	<div class="col-sm-9"> 
					      		<input type="text" name="ruc" class="form-control" onkeypress="if (event.keyCode < 48 || event.keyCode > 57) event.returnValue = false;" id="ruc"  maxlength=11 placeholder="Ingresa número de RUC" required="">
					    	</div>
					  	</div>
					  	<div class="form-group">
					    	<label class="control-label col-sm-3 text-primary" for="">Dirección:</label>
					    	<div class="col-sm-9">
					      		<input type="text" maxlength="50" class="form-control" id="dir" name="dir" placeholder="Ingresa dirección" required="" autofocus="">
					    	</div>
					  	</div>
					  	<div class="form-group">
					    	<label class="control-label col-sm-3 text-primary" for="">Teléfono:</label>
					    	<div class="col-sm-9">
					      		<input type="text" maxlength="9" class="form-control" id="tel" name="tel" required="" placeholder="Ingresa número de teléfono" autofocus="">
					    	</div>
					  	</div>
					  	<div class="form-group">
					    	<label class="control-label col-sm-3 text-primary" for="">Celular:</label>
				    		<div class="col-sm-9">
					      		<input type="text" maxlength="9" class="form-control" id="cel" name="cel" required="" placeholder="Ingresa número de Celular" autofocus="">
					    	</div>
					  	</div>
					  	<div class="form-group">
					    	<label class="control-label col-sm-3 text-primary" for="">Email:</label>
					    	<div class="col-sm-9">
					      		<input type="email" maxlength="50" class="form-control" id="mail" name="mail" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}" placeholder="Ingresa email" required="" autofocus="">
					    	</div>
					  	</div>
					</div>
				</div>
				<div class="modal-footer">
					<center>
	                	<button type="button" class="btn btn-white btn-warning btn-sm btn-round" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancelar</button>
	                	<button type="submit" name="editar" class="btn btn-white btn-success btn-sm btn-round"><span class="glyphicon glyphicon-check"></span> Actualizar</button>
	                </center>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
	function validaNumericos(event) {
    if(event.charCode >= 48 && event.charCode <= 57){
      return true;
     }
     return false;        
}
var mostrarValor = function(x){
    if (x=='DNI') {
        document.getElementById('doc').maxLength=8;
        document.getElementById('doc').readOnly=false;
        document.getElementById('doc').focus();
    }else{
    	document.getElementById('doc').maxLength=11;
    	document.getElementById('doc').readOnly=false;
        document.getElementById('doc').focus();
    }
}
 function soloLetras(e){
       key = e.keyCode || e.which;
       tecla = String.fromCharCode(key).toLowerCase();
       letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
       especiales = "8-37-39-46";

       tecla_especial = false
       for(var i in especiales){
            if(key == especiales[i]){
                tecla_especial = true;
                break;
            }
        }

        if(letras.indexOf(tecla)==-1 && !tecla_especial){
            return false;
        }
    }
</script>