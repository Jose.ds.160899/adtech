<div class="modal fade" id="edit_<?php echo $proveedor[0]; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        	<div class="panel panel-primary">
	            <div class="panel-heading">
	                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                <h3 class="panel-title"><span class="glyphicon glyphicon-list" aria-hidden="true"></span> Editar Proveedor</h3>
	            </div>
	        </div>
	        <form class="form-horizontal" method="POST" action="guardar_proveedor.php">
	            <div class="modal-body">
	            	<input type="hidden" name="proveedor_id" value="<?php echo $proveedor[0] ?>">
					<input type="hidden" name="actualiza" value="1">
	            	<div class="container-fluid">
						<div class="row form-group">
							<div class="col-sm-3">
								<label class="control-label text-primary" style="position:relative; top:7px;">R. Social:</label>
							</div>
							<div class="col-sm-9">
								<input type="text" maxlength="100" class="form-control" id="r_social" name="r_social" value="<?php echo $proveedor[2];?>"  onkeypress="return soloLetras(event)" required autofocus="">
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-3">
								<label class="control-label text-primary" style="position:relative; top:7px;">RUC:</label>
							</div>
							<div class="col-sm-9">
								<input type="text" name="ruc" class="form-control" minlength="11" onkeypress="if (event.keyCode < 48 || event.keyCode > 57) event.returnValue = false;" id="ruc"  maxlength=11 value="<?php echo $proveedor[1] ?>" required="">
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-3">
								<label class="control-label text-primary" style="position:relative; top:7px;">Dirección:</label>
							</div>
							<div class="col-sm-9">
								<input type="text" maxlength="50" class="form-control" id="dir" name="dir" value="<?php echo $proveedor[3] ?>" required="" autofocus="">
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-3">
								<label class="control-label text-primary" style="position:relative; top:7px;">Teléfono:</label>
							</div>
							<div class="col-sm-9">
								<input type="text" maxlength="9" class="form-control" id="tel" name="tel" required="" value="<?php echo $proveedor[4] ?>" autofocus="">
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-3">
								<label class="control-label text-primary" style="top:7px;">Celular:</label>
							</div>
							<div class="col-sm-9">
								<input type="text" maxlength="9" class="form-control" minlength="9" id="cel" name="cel" required value="<?php echo $proveedor[5] ?>" autofocus="">
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-3">
								<label class="control-label text-primary" style="position:relative; top:7px;">Email:</label>
							</div>
							<div class="col-sm-9">
								<input type="email" maxlength="50" class="form-control" id="mail" name="mail" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}" value="<?php echo $proveedor[6] ?>" required="" autofocus="">
							</div>
						</div>
						<?php if ($proveedor[7]==0) { ?>
				  		<div class="row form-group">
							<div class="col-sm-3">
								<label class="control-label text-primary" style="position:relative; top:7px;">Estado:</label>
							</div>
							<div class="col-sm-9">
								<label class="switch">
									<input type="checkbox" class="form-control" name="estado" <?php if ($proveedor[7]==1) { echo "checked";} ?> value="1">
									<span class="slider round"></span>
								</label>	
							</div>
						</div><?php } ?>
					</div>
				</div>
	            <div class="modal-footer">
	                <center><button type="button" class="btn btn-white btn-warning btn-sm btn-round" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancelar</button>
	                <button type="submit" name="editar" class="btn btn-white btn-success btn-sm btn-round"><span class="glyphicon glyphicon-check"></span> Actualizar</button></center>
	            </div>
			</form>
            </div>
        </div>
    </div>
</div>
<!-- Delete -->
<div class="modal fade" id="delete_<?php echo $proveedor[0]; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="panel panel-warning">
				<div class="panel-heading">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                <h3 class="panel-title"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Eliminar Proveedor</h3>
	            </div>
	        </div>
            <div class="modal-body">    
                <h5 class="text-center text-danger">Esta seguro de eliminar el Proveedor:</h5>
                <h6 class="text-center text-primary"><?php echo $proveedor[2]; ?></h6>
            </div>
            <div class="modal-footer center">
                <button type="button" class="btn btn-white btn-warning btn-sm btn-round" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancelar</button>
                <a href="guardar_proveedor.php?id=<?php echo $proveedor[0]; ?>" class="text-success"><button type="submit" name="editar" class="btn btn-white btn-success btn-sm btn-round"><span class="glyphicon glyphicon-trash"></span> Eliminar</button></a></center>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
	function validaNumericos(event) {
    if(event.charCode >= 48 && event.charCode <= 57){
      return true;
     }
     return false;        
}
var mostrarValor = function(x){
    if (x=='DNI') {
        document.getElementById('doc').maxLength=8;
        document.getElementById('doc').readOnly=false;
        document.getElementById('doc').focus();
    }else{
    	document.getElementById('doc').maxLength=11;
    	document.getElementById('doc').readOnly=false;
        document.getElementById('doc').focus();
    }
}
 function soloLetras(e){
       key = e.keyCode || e.which;
       tecla = String.fromCharCode(key).toLowerCase();
       letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
       especiales = "8-37-39-46";

       tecla_especial = false
       for(var i in especiales){
            if(key == especiales[i]){
                tecla_especial = true;
                break;
            }
        }

        if(letras.indexOf(tecla)==-1 && !tecla_especial){
            return false;
        }
    }
</script>