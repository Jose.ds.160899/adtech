-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 26-06-2022 a las 03:35:59
-- Versión del servidor: 10.4.22-MariaDB
-- Versión de PHP: 8.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `adtech_bd`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `anulados`
--

CREATE TABLE `anulados` (
  `Id_anulado` int(11) NOT NULL,
  `Id_client` int(11) NOT NULL,
  `Id_art` int(11) NOT NULL,
  `Id_vta` int(11) NOT NULL,
  `Id_user` int(11) NOT NULL,
  `Fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulo`
--

CREATE TABLE `articulo` (
  `Id_art` int(11) NOT NULL,
  `Id_tipo_art` int(11) NOT NULL,
  `idcat` int(11) NOT NULL,
  `Codigo` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Nombre` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `Precio` decimal(11,2) NOT NULL,
  `Composicion` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Fabricante` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `p_compra` decimal(11,2) NOT NULL,
  `Estado` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `articulo`
--

INSERT INTO `articulo` (`Id_art`, `Id_tipo_art`, `idcat`, `Codigo`, `Nombre`, `Precio`, `Composicion`, `Fabricante`, `p_compra`, `Estado`) VALUES
(3, 1, 1, '1', 'prueba', '1.50', NULL, '1', '1.20', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carrito`
--

CREATE TABLE `carrito` (
  `Id_cart` int(11) NOT NULL,
  `Id_art` int(11) NOT NULL,
  `Cant_cart` int(11) NOT NULL,
  `Precio_art` decimal(11,2) NOT NULL,
  `Id_stock` int(11) NOT NULL,
  `Id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `carrito`
--

INSERT INTO `carrito` (`Id_cart`, `Id_art`, `Cant_cart`, `Precio_art`, `Id_stock`, `Id_user`) VALUES
(4, 3, 1, '1.50', 1, 1);

--
-- Disparadores `carrito`
--
DELIMITER $$
CREATE TRIGGER `tr_nuevo_stock` BEFORE INSERT ON `carrito` FOR EACH ROW BEGIN
UPDATE stock SET Stock= Stock - NEW.cant_cart
WHERE stock.id_stock=NEW.id_stock;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `tr_retornar_stock` AFTER DELETE ON `carrito` FOR EACH ROW BEGIN
UPDATE stock SET Stock=Stock + old.Cant_cart
WHERE stock.id_stock = old.id_stock;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `tr_update_carrito` BEFORE UPDATE ON `carrito` FOR EACH ROW BEGIN
UPDATE stock SET Stock= Stock - NEW.cant_cart
WHERE stock.id_stock=NEW.id_stock;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `idcat` int(11) NOT NULL,
  `categoria` text COLLATE utf8_spanish_ci NOT NULL,
  `fecha` date NOT NULL,
  `estado` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`idcat`, `categoria`, `fecha`, `estado`) VALUES
(1, 'Epoxico', '2022-05-28', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `Id_client` int(11) NOT NULL,
  `Tipo_client` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `Nom_client` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `Tipo_doc` char(5) COLLATE utf8_spanish_ci NOT NULL,
  `Num_doc` char(11) COLLATE utf8_spanish_ci NOT NULL,
  `Direccion` varchar(70) COLLATE utf8_spanish_ci NOT NULL,
  `Fono` int(9) NOT NULL,
  `Email` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `limite_credito` double(8,2) DEFAULT NULL,
  `Estado` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`Id_client`, `Tipo_client`, `Nom_client`, `Tipo_doc`, `Num_doc`, `Direccion`, `Fono`, `Email`, `limite_credito`, `Estado`) VALUES
(1, 'Cliente', 'MARIO', 'DNI', '41698740', 'DON ARCADIO', 965346320, 'mariomd1512@gmail.com', 1500.00, 1),
(2, 'Cliente', 'MARIO', 'DNI', '1111', 'DON ARCADIO', 965346320, 'mariomd1512@gmail.com', 343.00, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `correlativo`
--

CREATE TABLE `correlativo` (
  `Id_corr` int(11) NOT NULL,
  `Tipo_comp` char(7) COLLATE utf8_spanish_ci NOT NULL,
  `Num_comp` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `correlativo`
--

INSERT INTO `correlativo` (`Id_corr`, `Tipo_comp`, `Num_comp`) VALUES
(1, 'Ticket', 1),
(2, 'Boleta', 1),
(3, 'Factura', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `det_venta`
--

CREATE TABLE `det_venta` (
  `Id_det_vta` int(11) NOT NULL,
  `Id_vta` int(11) NOT NULL,
  `Id_art` int(11) NOT NULL,
  `Cant` int(11) NOT NULL,
  `Precio_vta` decimal(11,2) NOT NULL,
  `Dscto` decimal(11,2) NOT NULL,
  `Estado` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `deudas`
--

CREATE TABLE `deudas` (
  `Id_deuda` int(11) NOT NULL,
  `Id_user` int(11) NOT NULL,
  `Id_client` int(11) NOT NULL,
  `Id_vta` int(11) NOT NULL,
  `Fecha` date NOT NULL,
  `Hora` time NOT NULL,
  `Moneda` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `Cant_dias` int(11) NOT NULL,
  `Total` double(10,2) NOT NULL,
  `Estado` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `deudas`
--

INSERT INTO `deudas` (`Id_deuda`, `Id_user`, `Id_client`, `Id_vta`, `Fecha`, `Hora`, `Moneda`, `Cant_dias`, `Total`, `Estado`) VALUES
(1, 1, 1, 1, '2022-06-18', '20:17:59', 'SOLES', 30, 250.00, 1),
(2, 1, 1, 1, '2022-06-21', '20:17:59', 'SOLES', 30, 250.00, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagos_cliente`
--

CREATE TABLE `pagos_cliente` (
  `Id_pago` int(11) NOT NULL,
  `Id_user` int(11) NOT NULL,
  `Id_client` int(11) NOT NULL,
  `Id_vta` int(11) NOT NULL,
  `Fecha` datetime NOT NULL,
  `Hora` time NOT NULL,
  `Pago` double(10,2) NOT NULL,
  `Estado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagos_prov`
--

CREATE TABLE `pagos_prov` (
  `Id_pagos` int(9) NOT NULL,
  `Id_Prov` int(9) NOT NULL,
  `Fecha` date DEFAULT NULL,
  `F_pago` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Cant` double(9,2) DEFAULT NULL,
  `N_boleta` varchar(15) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Estado` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permiso`
--

CREATE TABLE `permiso` (
  `idpermiso` int(11) NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `permiso`
--

INSERT INTO `permiso` (`idpermiso`, `nombre`) VALUES
(1, 'Ventas'),
(2, 'Inventario'),
(3, 'Clientes'),
(4, 'Compras'),
(5, 'Reportes'),
(6, 'Accesos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos_vencidos`
--

CREATE TABLE `productos_vencidos` (
  `Id_vec` int(11) NOT NULL,
  `Nom_venc` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `Precio_venc` decimal(11,2) NOT NULL,
  `Cant_venc` int(11) NOT NULL,
  `Fecha_exp` date NOT NULL,
  `Fecha_reg` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor`
--

CREATE TABLE `proveedor` (
  `Id_prov` int(9) NOT NULL,
  `Ruc` varchar(11) COLLATE utf8_spanish_ci DEFAULT NULL,
  `R_Social` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Direccion` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fono` char(9) COLLATE utf8_spanish_ci DEFAULT NULL,
  `cel` varchar(9) COLLATE utf8_spanish_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Estado` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `proveedor`
--

INSERT INTO `proveedor` (`Id_prov`, `Ruc`, `R_Social`, `Direccion`, `fono`, `cel`, `email`, `Estado`) VALUES
(1, '10416987403', 'MARIO MENDOZA', 'PUEBLO NEUVO', '965346320', '965346320', 'MARIOMD@GMAIL.COM', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `stock`
--

CREATE TABLE `stock` (
  `Id_stock` int(11) NOT NULL,
  `Id_art` int(11) NOT NULL,
  `Stock` int(11) NOT NULL,
  `Fecha_compro` date NOT NULL,
  `Fecha_Venc` date DEFAULT NULL,
  `Id_user` int(11) NOT NULL,
  `Fecha_reg` date NOT NULL,
  `Estado` int(2) NOT NULL,
  `Ingreso_compra` int(9) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `stock`
--

INSERT INTO `stock` (`Id_stock`, `Id_art`, `Stock`, `Fecha_compro`, `Fecha_Venc`, `Id_user`, `Fecha_reg`, `Estado`, `Ingreso_compra`) VALUES
(1, 3, 99, '2002-05-10', '2050-01-01', 1, '2022-06-15', 1, 100);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_articulo`
--

CREATE TABLE `tipo_articulo` (
  `Id_tipo_art` int(11) NOT NULL,
  `Nom_tipo_art` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `Tipo_presenta` varchar(5) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tipo_articulo`
--

INSERT INTO `tipo_articulo` (`Id_tipo_art`, `Nom_tipo_art`, `Tipo_presenta`) VALUES
(1, 'UNIDAD', 'UN'),
(30, 'CAJA', 'CJ');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `Id_user` int(11) NOT NULL,
  `Nombre` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `Tipo_doc` varchar(5) COLLATE utf8_spanish_ci NOT NULL,
  `Num_doc` char(11) COLLATE utf8_spanish_ci NOT NULL,
  `Direccion` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `Fono` varchar(9) COLLATE utf8_spanish_ci NOT NULL,
  `Email` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `Cargo` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `Clave` varchar(8) COLLATE utf8_spanish_ci NOT NULL,
  `Estado` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`Id_user`, `Nombre`, `Tipo_doc`, `Num_doc`, `Direccion`, `Fono`, `Email`, `Cargo`, `Clave`, `Estado`) VALUES
(1, 'Mario Mendoza Dianderas', 'DNI', '41698740', 'Chincha', '9653463', 'mariomd1512@gmail.com', 'admin', 'admin', 1),
(2, 'LIZBETH VELIZ', 'DNI', '42213866', 'DON ARCADIO', '945414918', 'mariomd1512@gmail.com', 'ADMINISTRADOR', '23423434', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario_permiso`
--

CREATE TABLE `usuario_permiso` (
  `idusuario_permiso` int(9) NOT NULL,
  `id_user` int(11) NOT NULL,
  `idpermiso` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuario_permiso`
--

INSERT INTO `usuario_permiso` (`idusuario_permiso`, `id_user`, `idpermiso`) VALUES
(1, 2, 1),
(2, 2, 5),
(3, 2, 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta`
--

CREATE TABLE `venta` (
  `Id_vta` int(11) NOT NULL,
  `Id_clie` int(11) NOT NULL,
  `Id_user` int(11) NOT NULL,
  `Tipo_comprob` char(5) COLLATE utf8_spanish_ci NOT NULL,
  `num_comprob` varchar(11) COLLATE utf8_spanish_ci NOT NULL,
  `moneda` varchar(10) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Fecha` date NOT NULL,
  `Hora` time NOT NULL,
  `Impuesto` decimal(11,2) NOT NULL,
  `subtotal` double(10,2) DEFAULT NULL,
  `Total_vta` decimal(11,2) NOT NULL,
  `T_pago` varchar(4) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Estado` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Disparadores `venta`
--
DELIMITER $$
CREATE TRIGGER `tr_correlativo` AFTER INSERT ON `venta` FOR EACH ROW BEGIN
UPDATE correlativo SET Num_comp=Num_comp+ 1
WHERE Tipo_comp = 'Ticket';
END
$$
DELIMITER ;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `anulados`
--
ALTER TABLE `anulados`
  ADD PRIMARY KEY (`Id_anulado`);

--
-- Indices de la tabla `articulo`
--
ALTER TABLE `articulo`
  ADD PRIMARY KEY (`Id_art`),
  ADD UNIQUE KEY `fk_tipo_articulo` (`Id_tipo_art`),
  ADD KEY `fk_categoria` (`idcat`);

--
-- Indices de la tabla `carrito`
--
ALTER TABLE `carrito`
  ADD PRIMARY KEY (`Id_cart`);

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`idcat`);

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`Id_client`);

--
-- Indices de la tabla `correlativo`
--
ALTER TABLE `correlativo`
  ADD PRIMARY KEY (`Id_corr`);

--
-- Indices de la tabla `det_venta`
--
ALTER TABLE `det_venta`
  ADD PRIMARY KEY (`Id_det_vta`);

--
-- Indices de la tabla `deudas`
--
ALTER TABLE `deudas`
  ADD PRIMARY KEY (`Id_deuda`);

--
-- Indices de la tabla `pagos_cliente`
--
ALTER TABLE `pagos_cliente`
  ADD PRIMARY KEY (`Id_pago`);

--
-- Indices de la tabla `pagos_prov`
--
ALTER TABLE `pagos_prov`
  ADD PRIMARY KEY (`Id_pagos`);

--
-- Indices de la tabla `permiso`
--
ALTER TABLE `permiso`
  ADD PRIMARY KEY (`idpermiso`);

--
-- Indices de la tabla `productos_vencidos`
--
ALTER TABLE `productos_vencidos`
  ADD PRIMARY KEY (`Id_vec`);

--
-- Indices de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD PRIMARY KEY (`Id_prov`);

--
-- Indices de la tabla `stock`
--
ALTER TABLE `stock`
  ADD PRIMARY KEY (`Id_stock`);

--
-- Indices de la tabla `tipo_articulo`
--
ALTER TABLE `tipo_articulo`
  ADD PRIMARY KEY (`Id_tipo_art`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`Id_user`);

--
-- Indices de la tabla `usuario_permiso`
--
ALTER TABLE `usuario_permiso`
  ADD PRIMARY KEY (`idusuario_permiso`),
  ADD KEY `fk_usuario` (`id_user`),
  ADD KEY `fk_permiso` (`idpermiso`);

--
-- Indices de la tabla `venta`
--
ALTER TABLE `venta`
  ADD PRIMARY KEY (`Id_vta`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `anulados`
--
ALTER TABLE `anulados`
  MODIFY `Id_anulado` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `articulo`
--
ALTER TABLE `articulo`
  MODIFY `Id_art` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `carrito`
--
ALTER TABLE `carrito`
  MODIFY `Id_cart` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `idcat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `Id_client` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `correlativo`
--
ALTER TABLE `correlativo`
  MODIFY `Id_corr` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `det_venta`
--
ALTER TABLE `det_venta`
  MODIFY `Id_det_vta` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `deudas`
--
ALTER TABLE `deudas`
  MODIFY `Id_deuda` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `pagos_cliente`
--
ALTER TABLE `pagos_cliente`
  MODIFY `Id_pago` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `pagos_prov`
--
ALTER TABLE `pagos_prov`
  MODIFY `Id_pagos` int(9) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `permiso`
--
ALTER TABLE `permiso`
  MODIFY `idpermiso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `productos_vencidos`
--
ALTER TABLE `productos_vencidos`
  MODIFY `Id_vec` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  MODIFY `Id_prov` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `stock`
--
ALTER TABLE `stock`
  MODIFY `Id_stock` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `tipo_articulo`
--
ALTER TABLE `tipo_articulo`
  MODIFY `Id_tipo_art` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `Id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `usuario_permiso`
--
ALTER TABLE `usuario_permiso`
  MODIFY `idusuario_permiso` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `venta`
--
ALTER TABLE `venta`
  MODIFY `Id_vta` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `articulo`
--
ALTER TABLE `articulo`
  ADD CONSTRAINT `articulo_ibfk_1` FOREIGN KEY (`idcat`) REFERENCES `categorias` (`idcat`),
  ADD CONSTRAINT `articulo_ibfk_2` FOREIGN KEY (`Id_tipo_art`) REFERENCES `tipo_articulo` (`Id_tipo_art`);

--
-- Filtros para la tabla `usuario_permiso`
--
ALTER TABLE `usuario_permiso`
  ADD CONSTRAINT `usuario_permiso_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `usuario` (`Id_user`),
  ADD CONSTRAINT `usuario_permiso_ibfk_2` FOREIGN KEY (`idpermiso`) REFERENCES `permiso` (`idpermiso`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
