<?php 
    session_start();
#error_reporting(0);
require_once 'conexion.php';
$ticket=$_GET['ticket'];
$rs_carrito=mysqli_fetch_row(mysqli_query($conex,"SELECT * FROM venta WHERE num_comprob='$ticket'"));
$id_vta=$rs_carrito[0];
$rs_venta=mysqli_query($conex,"SELECT * FROM det_venta WHERE Id_vta='$id_vta'");
$fecha=$rs_carrito[6];
$hora=$rs_carrito[7];
$fpago=$rs_carrito[10];
$tcomp=$rs_carrito[3];
$rs_tcomp=mysqli_fetch_row(mysqli_query($conex,"SELECT * FROM correlativo WHERE Id_corr='$tcomp'"));
$id_cliente=$rs_carrito[1];
$rs_cliente=mysqli_fetch_row(mysqli_query($conex,"SELECT * FROM clientes WHERE Id_client='$id_cliente'"));
$rs_empresa=mysqli_fetch_row(mysqli_query($conex,"SELECT * FROM empresa WHERE Estado=1"));
$nom_clie=$rs_cliente[2];
$dni_clie=$rs_cliente[4];
 ?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Ticket de Venta</title>
    <!-- Bootstrap Core CSS -->
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap-theme.min.css">
    <link href="../assets/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <script type="text/javascript">
        print();
    </script>
    <style type="text/css">
        h2,h3{
            font-family: 'Verdana';
        }
    </style>
</head>
<body onload="window.print();">
	<?php 
//establecemos los datos de la empresa
$empresa = $rs_empresa[1];
$ruc = $rs_empresa[3];
$direccion = $rs_empresa[4];
$telefono = $rs_empresa[5];
$email = $rs_empresa[6];
	 ?>
<div class="table-responsive" id="print-ticket">
	<!--codigo imprimir-->
	<br>
	<table border="0" align="center" width="300px">
		<tr>
			<td align="center">
				<!--mostramos los datos de la empresa en el doc HTML-->
				.:: <strong><?php echo $empresa ?></strong> ::.<br>
				<?php echo $ruc ?><br>
				<?php echo $direccion ?> <br><br>
			</td>
		</tr>
		<tr>
			<td align="center">
			 <b><?php echo strtoupper($rs_tcomp[1]) ?> DE VENTA<br> 
			 <?php echo $ticket; ?></b><hr>
			</td>
		</tr>
		<tr>
			<td><b>Fecha de Emisión: </b> <?php echo $fecha; ?></td>
		</tr>
		<tr>
			<td><b>Hora de Emisión: </b> <?php echo $hora; ?></td>
		</tr>
		<tr>
			<td><b>Forma de pago: </b> <?php echo $fpago; ?></td>
		</tr>
		<tr> 
			<td align="center"></td>
		</tr>
		<tr>
			<!--mostramos los datos del cliente -->
			<td><b>Cliente: </b><?php echo $nom_clie; ?>
			</td>
		</tr>
		<tr>
			<td>
				<b>DNI: </b><?php echo $dni_clie; ?><hr>
			</td>
		</tr>
		
	</table>
	<!--mostramos lod detalles de la venta -->
	<table border="0" align="center" width="200px">
			<tr>
				<td><b>Cant.</b></td>
				<td><center><b>Descripción</b></center></td>
				<td align="right"><b>Importe</b></td>
			</tr>
			<tr>
				<td colspan="3">=================================</td>
			</tr>
			<?php
                $total = 0;
                $i=1;
                /*$rows=mysqli_num_rows($rs_carrito);  */
                while ($carrito=mysqli_fetch_row($rs_venta)) {
                	$id_prod=$carrito[2];
                	$rs_producto=mysqli_fetch_row(mysqli_query($conex,"SELECT * FROM articulo WHERE Id_art='$id_prod'"));
                    $precio=$carrito[4];
                    $cant=$carrito[3];
                    $subtotal=$precio*$cant;
                    $total += $subtotal;
                  ?>
                <tr align="center">
                    <td><?php echo $cant; ?></td>
                    <td align="left"><?php echo ucwords($rs_producto[4]); ?></td>
                    <td><?php echo "S/ ". number_format($subtotal, 2); ?></td>   
                </tr>

            <?php $i++; }; 
            
            ?>
            <tr>
			<td colspan="3">=================================</td>
		</tr>	
		<tr>
			<td>&nbsp;</td>
			<td align="right"><b>TOTAL:</b></td>
			<td align="center"><b>S/. <?php echo number_format($total,2); ?></b></td>
		</tr>
		<tr>
			<td colspan="3">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="3" align="center">¡Gracias por su compra!</td>
		</tr>
	</table>
	<br>
	</table>
	<br>
</div>
<p>&nbsp;</p>
</body>
</html>
<!-- <?php #mysqli_query($conex,"TRUNCATE TABLE carrito"); ?> -->