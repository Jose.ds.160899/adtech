<?php
require_once 'conexion.php';
session_start();
$nomb=$_SESSION['nom'];
if ($nomb== null || $nomb=="") { ?>
  <div class="container"><br>
    <div class="alert alert-danger" role="alert"><strong>Atención! </strong>No tiene permiso</div>
  </div><?php
  header("location:index.html");
}
if(isset($_GET['id'])){
  $id_comp=$_GET['id'];
  mysqli_query($conex,"UPDATE correlativo SET Estado=0 WHERE Id_corr=$id_comp");
  echo '<script language="javascript">alert("Correlativo eliminado correctamente");window.location.href="tipo_documento.php";</script>';
}else{
  $fecha=date('Y-m-d');
  $nom_doc=$_POST['nombre'];
  $tipo_op=$_POST['tipo_op'];
  $estado=$_POST['estado'];
  $actualiza=$_POST['actualiza'];
  $id_corr=$_POST['id_corr'];
  try {
    if ($actualiza==1) {
      #echo '<script language="javascript">alert("A: '.$nom_doc.' B: '.$tipo_op.' C: '.$estado.' D: '.$id_corr.'");</script>';
      mysqli_query($conex,"UPDATE correlativo SET Tipo_comp='$nom_doc',Tipo_doc='$tipo_op',Estado=$estado WHERE Id_corr=$id_corr");
      header('location:tipo_documento.php');
    }else{
      $existe=mysqli_num_rows(mysqli_query($conex,"SELECT * FROM correlativo WHERE Tipo_comp='$nom_doc'"));
      /*echo '<script type="text/javascript">alert("'.$existe.'");</script>';*/
      if ($existe>=1) {
      echo '<script>alert("La Categoria ya se encuentra registrada");window.location="tipo_documento.php";</script>';
      }else{
        mysqli_query($conex,"INSERT INTO correlativo VALUES(0,'$nom_doc','$tipo_op',$estado)");
        header('location:tipo_documento.php');
      }  
    } 
  } catch (Exception $e) {
    echo 'Excepción capturada: ',  $e->getMessage(), "\n";
  }
}
mysqli_close($conex);
 ?>
