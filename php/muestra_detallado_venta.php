<?php
error_reporting(0);
session_start();
require_once 'conexion.php';
date_default_timezone_set('America/Lima');
$nomb=$_SESSION['nom'];
$valida=mysqli_num_rows(mysqli_query($conex,"SELECT Num_doc FROM usuario where Num_doc='$nomb'"));
if ($nomb== null or $nomb=="" or $valida==0) {
  header("location:../index.html");
}
if (isset($_POST['f_inicio'])) {
    $f_ini=$_POST['f_inicio'];    
}else{
    $f_ini=date('Y-m-d');
}
if (isset($_POST['f_fin'])) {
    $f_final=$_POST['f_fin'];
}else{
    $f_final=date('Y-m-d');
}
$usuario=mysqli_fetch_row(mysqli_query($conex,"SELECT * FROM usuario WHERE Num_doc='$nomb'"));
$rs_ventas=mysqli_query($conex,"SELECT a.Fecha,a.num_comprob,c.Nombre,b.Cant,b.Precio_vta,a.Estado FROM venta a INNER JOIN det_venta b ON a.Id_vta=b.Id_vta inner join articulo c ON b.Id_art=c.Id_art WHERE a.Fecha BETWEEN '$f_ini' AND '$f_final' AND a.Id_user='$usuario[0]' AND a.Estado=1");
?>
<!DOCTYPE html>
<html>
<head>
    <title></title>
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="../assets/font-awesome/4.5.0/css/font-awesome.min.css" />
    <!-- text fonts -->
    <link rel="stylesheet" href="../assets/css/fonts.googleapis.com.css" />
    <!-- ace styles -->
    <link rel="stylesheet" href="../assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />
    <link rel="stylesheet" href="../assets/css/ace-skins.min.css" />
    <link rel="stylesheet" href="../assets/css/ace-rtl.min.css" />
    <!-- ace settings handler -->
    <script src="../assets/js/ace-extra.min.js"></script>
</head>
<body>
  <div class="main-content">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> Reporte Detallado</h3>
            </div>
            <div class="table-responsive">
                <table id="detallado" class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr class="info center" >
                            <th>IT</th>
                            <!-- <th>Nombre</th> -->
                            <th>Fecha</th>
                            <th>Ticket</th>
                            <th>Producto</th>
                            <!-- <th>Presentación</th> -->
                            <th>Cantidad</th>
                            <th>Precio</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php $i=1; $total=0;
                    while ($rpte=mysqli_fetch_row($rs_ventas)) {
                        $precio=$rpte[4];
                        $estado=$rpte[5];
                        $subtotal=$precio;
                          $total += $subtotal;
                        ?>
                        <tr align="center">
                            <td><?php echo $i; ?></td>
                            <td><?php echo $rpte[0]; ?></td>
                            <td><?php echo $rpte[1]; ?></td>
                            <td align="left"><?php echo $rpte[2]; ?></td>
                            <td><?php echo $rpte[3]; ?></td>
                            <td><?php echo $rpte[4]; ?></td>
                        </tr><?php $i++;
                        }
                    ?>
                    </tbody>
                    <tr>
                      <td></td>
                        <td></td>
                        <td></td>
                      <td></td>
                      <td align="right"><b>Total: </b></td>
                        <td align="center"><b><?php echo number_format($total,2); ?></b></td>
                    </tr>
                </table>
            </div>
        </div>
    </div><!-- /.col -->
  </div>
  
  <script src="../assets/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript">
    if('ontouchstart' in document.documentElement) document.write("<script src='../assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
</script>
<!-- <script src="../assets/js/bootstrap.min.js"></script> -->

<!-- page specific plugin scripts -->
<script src="../assets/js/jquery.dataTables.min.js"></script>
<script src="../assets/js/jquery.dataTables.bootstrap.min.js"></script>
<script src="../assets/js/dataTables.buttons.min.js"></script>
<script src="../assets/js/buttons.flash.min.js"></script>
<script src="../assets/js/buttons.html5.min.js"></script>
<script src="../assets/js/buttons.print.min.js"></script>
<script src="../assets/js/jszip.min.js"></script>
<script src="../assets/js/buttons.colVis.min.js"></script>
<script src="../assets/js/dataTables.select.min.js"></script>

        <!-- ace scripts -->
        <script src="../assets/js/ace-elements.min.js"></script>
        <script src="../assets/js/ace.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#detallado').DataTable({
            "info": false,
            "language": {
            "url": "../assets/js/Spanish.json"
            },
            dom: 'Bfrtip',
            buttons: [
                {"extend": 'print', "text": '<span class = "glyphicon glyphicon-print"> </span>', "className": 'btn btn-success btn-xs'},
                {"extend": 'excel', "text": '<span class = "glyphicon glyphicon-list"></span>', "className": 'btn btn-success btn-xs'}
            ]
        });
    });
</script>
</body>
</html>