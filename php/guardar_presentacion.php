<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');
require_once 'conexion.php';
session_start();
$nomb=$_SESSION['nom'];
if ($nomb== null || $nomb=="") { ?>
  <div class="container"><br>
    <div class="alert alert-danger" role="alert"><strong>Atención! </strong>No tiene permiso</div>
  </div><?php
  header("location:index.html");
}
if(isset($_GET['id'])){
  $id_pres=$_GET['id'];
  mysqli_query($conex,"UPDATE tipo_articulo SET Estado=0 WHERE Id_tipo_art=$id_pres");
  echo '<script language="javascript">alert("Presentación eliminada correctamente");window.location.href="presentacion.php";</script>';
}else{
  $fecha=date('Y-m-d');
  $nombre=$_POST['nombre'];
  $ncorto=$_POST['ncorto'];
  /*$estado=$_POST['estado'];*/
  $estado=1;
  $actualiza=$_POST['actualiza'];
  $id_pre=$_POST['id_pre'];
  try {
    if ($actualiza==1) {
      mysqli_query($conex,"UPDATE tipo_articulo SET Nom_tipo_art='$nombre',Tipo_presenta='$ncorto',fecha='$fecha',estado='$estado' WHERE Id_tipo_art=$id_pre");
      header('location:presentacion.php');
    }else{
      $existe=mysqli_num_rows(mysqli_query($conex,"SELECT * FROM tipo_articulo WHERE Nom_tipo_art='$nombre'"));
      /*echo '<script type="text/javascript">alert("'.$existe.'");</script>';*/
      if ($existe>=1) {
      echo '<script>alert("El nombre ya se encuentra registrada");window.location="presentacion.php";</script>';
      }else{
        mysqli_query($conex,"INSERT INTO tipo_articulo VALUES(0,'$nombre','$ncorto','$fecha',$estado)");
        header('location:presentacion.php');
      }  
    } 
  } catch (Exception $e) {
    echo 'Excepción capturada: ',  $e->getMessage(), "\n";
  }
}
mysqli_close($conex);
 ?>
