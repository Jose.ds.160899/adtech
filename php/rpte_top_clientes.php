<?php
session_start();
require_once('conexion.php');
$nomb=$_SESSION['nom'];
date_default_timezone_set('America/Lima');
$rpte_menu=1;
$permiso="Rpte Ventas";
$rs_user=mysqli_fetch_row(mysqli_query($conex,"SELECT Id_user FROM usuario where Num_doc='$nomb'"));
$id_user=$rs_user[0];
$sql = mysqli_query($conex, "SELECT p.*, d.* FROM permiso p INNER JOIN usuario_permiso d ON p.idpermiso = d.idpermiso WHERE d.id_user = $id_user AND p.nombre = '$permiso'");
$existe = mysqli_fetch_all($sql);
if (empty($existe) && $id_user != 1) {
    header("Location: permisos.php");
}
require 'header.php';
$fecha=date('Y-m-d');
?>
<div class="main-content">
    <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#"><?php echo $usuario[7]; ?></a>
                </li>
                <li class="active">Reporte Top Clientes</li>
            </ul><!-- /.breadcrumb -->
        </div>

        <div class="page-content">
            <div class="row">
                <form method="POST" action="muestra_datos_top_clientes.php">
                    <div class="panel-body" style="margin-bottom: 4px;">
                        <div class="col-md-1">
                            <label class="control-label text-primary"><b> Moneda:</b></label>   
                        </div>
                        <div class="col-md-3">
                            <select class="chosen-select form-control" name="moneda" required>
                                <option disabled selected>Seleccionar</option>
                                <option value="SOL">Soles</option>
                                <option value="DOLAR">Dolares</option>
                            </select>
                        </div>
                        <div class="col-md-1">
                            <label class="control-label text-primary"><b> Inicio:</b></label>   
                        </div>
                        <div class="col-md-2">
                            <input class="form-control" type="date" name="f_inicio" max="<?php echo date('Y-m-d')?>" value="<?php echo date('Y-m-d')?>">
                        </div>
                        <div class="col-md-1">
                            <label class="control-label text-primary"><b> Fin:</b></label>   
                        </div>
                        <div class="col-md-2">
                            <input class="form-control" type="date" name="f_fin" max="<?php echo date('Y-m-d')?>" value="<?php echo date('Y-m-d')?>">
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-white btn-sm btn-success btn-round" name="procesa" formtarget="mostrarconsulta">Consultar <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span></button>   
                        </div>
                    </div>
                </form>
                <div class="w3-row">
                    <object type="text/html" data="muestra_datos_top_clientes.php"  name="mostrarconsulta" width="100%" height="600px"></object>
                    
                </div>
                
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->

<script src="../assets/js/jquery-2.1.4.min.js"></script>
<script src="../assets/js/chosen.jquery.min.js"></script>
<script type="text/javascript">
    if('ontouchstart' in document.documentElement) document.write("<script src='../assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
</script>
<script src="../assets/js/bootstrap.min.js"></script>

<!-- page specific plugin scripts -->
<script src="../assets/js/jquery.dataTables.min.js"></script>
<script src="../assets/js/jquery.dataTables.bootstrap.min.js"></script>
<script src="../assets/js/dataTables.buttons.min.js"></script>
<script src="../assets/js/buttons.flash.min.js"></script>
<script src="../assets/js/buttons.html5.min.js"></script>
<script src="../assets/js/buttons.print.min.js"></script>
<script src="../assets/js/buttons.colVis.min.js"></script>
<script src="../assets/js/dataTables.select.min.js"></script>

<!-- ace scripts -->
<script src="../assets/js/ace-elements.min.js"></script>
<script src="../assets/js/ace.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#t_client').DataTable({
            "info": false,
            "language": {
            "url": "../assets/js/Spanish.json"
            }
        });
    });
    jQuery(function($) {
        if(!ace.vars['touch']) {
            $('.chosen-select').chosen({allow_single_deselect:true,width: '100%'});
            //resize the chosen on window resize

            $('#modal-stock').on('shown.bs.modal', function () {
                    if(!ace.vars['touch']) {
                        $(this).find('.chosen-container').each(function(){
                            $(this).find('a:first-child').css('width' , '100%');
                            $(this).find('.chosen-drop').css('width' , '100%');
                            $(this).find('.chosen-search input').css('width' , '100%');
                        });
                    }
                })
        }
    });
</script>
</body>
</html>
<?php
require 'footer.html';
?>
