<?php
require_once 'conexion.php';
session_start();
$nomb=$_SESSION['nom'];
if ($nomb== null || $nomb=="") { ?>
  <div class="container"><br>
    <div class="alert alert-danger" role="alert"><strong>Atención! </strong>No tiene permiso</div>
  </div><?php
  header("location:index.html");
}
if(isset($_GET['id'])){
  $id_prov=$_GET['id'];
  mysqli_query($conex,"UPDATE proveedor SET Estado=0 WHERE Id_prov=$id_prov");
  echo '<script language="javascript">alert("Proveedor eliminado correctamente");window.location.href="proveedores.php";</script>';
}else{
  $r_social=$_POST['r_social'];
  $ruc=$_POST['ruc'];
  $dir=$_POST['dir'];
  $tel=$_POST['tel'];
  $cel=$_POST['cel'];
  $mail=$_POST['mail'];
  $estado=1;
  if (isset($_POST['actualiza'])) {
    $actualiza=$_POST['actualiza'];
  }
  if (isset($_POST['proveedor_id'])) {
    $proveedor_id=$_POST['proveedor_id'];
  }
  try {
    if ($actualiza==1) {
      mysqli_query($conex,"UPDATE proveedor SET Ruc='$ruc',R_Social='$r_social',Direccion='$dir',Fono='$tel',cel=$cel,email='$mail', Estado=$estado WHERE Id_prov=$proveedor_id");
      header('location:proveedores.php');
    }else{
      $existe=mysqli_num_rows(mysqli_query($conex,"SELECT * FROM proveedor WHERE Ruc='$ruc'"));
      if ($existe>=1) {
        echo '<script>alert("El RUC ya se encuentra registrado");window.location="proveedores.php";</script>';
      }else{
        mysqli_query($conex,"INSERT INTO proveedor VALUES(0,'$ruc','$r_social','$dir',$tel,$cel,'$mail',1)");
        header('location:proveedores.php');
      }
    }
  } catch (Exception $e) {
    echo 'Excepción capturada: ',  $e->getMessage(), "\n";
  }
}
mysqli_close($conex);
 ?>
