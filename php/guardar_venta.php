<?php
require_once 'conexion.php';
session_start();
date_default_timezone_set('America/Lima');
$nomb=$_SESSION['nom'];
if ($nomb== null || $nomb=="") { ?>
  <div class="container"><br>
    <div class="alert alert-danger" role="alert"><strong>Atención! </strong>No tiene permiso</div>
  </div><?php
  header("location:index.html");
}
$rs_iduser=mysqli_fetch_row(mysqli_query($conex,"SELECT * FROM usuario WHERE Num_doc='$nomb'"));
$row=$_POST['row'];
$rs_carrito=mysqli_query($conex,"SELECT * FROM carrito");
$id_clie=$_POST['id_clie'];
$tipo_vta=$_POST['tipo_vta'];
$id_user=$rs_iduser[0];
$tipo_comp=$_POST['corr'];
$moneda = $_POST['cboMoneda'];
$serie=$_POST['dato'];
$correlativo=$_POST['ncorr'];
#$correlativo="1";
$num_comp=$serie.'-'.$correlativo;
$fecha=date('Y-m-d');
$hora=date("G:i:s");
$total_Vta=$_POST['tot_vta'];
$total=0;
#echo '<script language="javascript">alert("corre:'.$tipo_comp.' '.$num_comp.'");window.location="home.php";</script>';
try {
  $igv=0.18;
  if ($tipo_comp==5) {
    $subtotal=round($total_Vta/1.18,2);
    $impuesto=$subtotal*0.18;
  }else{
    $impuesto=0;
  }
  mysqli_query($conex,"INSERT INTO venta VALUES(NULL,$id_clie,$id_user,$tipo_comp,'$num_comp','$moneda','$fecha','$hora',$impuesto,$total_Vta,'$tipo_vta',1)");
  $rs_venta=mysqli_fetch_row(mysqli_query($conex,"SELECT * FROM venta order by id_vta desc limit 1"));
  $id_venta=$rs_venta[0];
  while ($det_art=mysqli_fetch_row($rs_carrito)) {
    $precio=($det_art[2] * $det_art[3]);
    $precio_det=($det_art[3]);
    mysqli_query($conex,"INSERT INTO det_venta values(0,$rs_venta[0],$det_art[1],$det_art[2],$precio_det,0,1)");
    $total=$total+($precio);
  }
  mysqli_query($conex,"UPDATE venta SET Total_vta=$total WHERE Id_vta=$id_venta");
  mysqli_query($conex,"TRUNCATE TABLE carrito");
  if ($tipo_vta=='CRED') {
    mysqli_query($conex,"INSERT INTO deudas values(0,$id_clie,$id_venta,'$fecha','$hora','$moneda','$total_Vta',1)");
  }
  echo '<script language="javascript">alert("Datos registrados correctamente");window.location="home.php";</script>';
  /*echo "<script language='javascript'>window.open('print-ticket.php?ticket=".$num_comp."','','width=520,height=480,left=50,top=50,toolbar=yes,resizable=no,location=no');window.location='home.php'</script>";*/

  #header('location:home.php');      
} catch (Exception $e) {
  echo 'Excepción capturada: ',  $e->getMessage(), "\n";
}
mysqli_close($conex);
?>
