<?php
session_start();
require_once('conexion.php');
date_default_timezone_set('America/Lima');
$presentacion_menu=1;
$nomb=$_SESSION['nom'];
/*$valida=mysqli_num_rows(mysqli_query($conex,"SELECT Num_doc FROM usuario where Num_doc='$nomb'"));
if ($nomb== null or $nomb=="" or $valida==0) {
  header("location:../index.html");
}*/
$permiso="Presentacion";
$rs_user=mysqli_fetch_row(mysqli_query($conex,"SELECT Id_user FROM usuario where Num_doc='$nomb'"));
$id_user=$rs_user[0];
$sql = mysqli_query($conex, "SELECT p.*, d.* FROM permiso p INNER JOIN usuario_permiso d ON p.idpermiso = d.idpermiso WHERE d.id_user = $id_user AND p.nombre = '$permiso'");
$existe = mysqli_fetch_all($sql);
if (empty($existe) && $id_user != 1) {
    header("Location: permisos.php");
}
include 'header.php';
include '../modal/nuevo_presentacion.php';
$rs_presentacion=mysqli_query($conex,"SELECT * FROM tipo_articulo");
?>
<style type="text/css">
    .disabled {
        cursor: not-allowed;
        pointer-events: none;
    }
</style>
<div class="main-content">
    <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">Home</a>
                </li>
                <li class="active">Presentación</li>
            </ul><!-- /.breadcrumb -->
        </div>

        <div class="page-content">
            <div class="page-header">
                <button type="button" class="btn btn-white btn-sm btn-success btn-round" data-toggle="modal" data-target="#modal-presentacion">Agregar Presentación <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span></button>

            </div><!-- /.page-header -->
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><span class="glyphicon glyphicon-list" aria-hidden="true"></span> Registro de Presentaciones</h3>
                        </div>
                        <div class="table-responsive">
                            <table id="t_client" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr class="info">
                                        <th>Item</th>
                                        <th>Nombre</th>
                                        <th>Nombre Corto</th>
                                        <th>Fecha Creación</th>
                                        <th>Estado</th>
                                        <th>OP</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                while ($presentacion=mysqli_fetch_row($rs_presentacion)) { ?>
                                    <tr>
                                        <td><?php echo $presentacion[0]; ?></td>
                                        <td><?php echo $presentacion[1]; ?></td>
                                        <td><?php echo $presentacion[2]; ?></td>
                                        <td><?php echo $presentacion[3]; ?></td>
                                        <td class="center"><?php
                                              if ($presentacion[4]==1) {
                                                echo '<span style="font-size: 10px;" class="label label-success arrowed-in arrowed-in-right">Activo</span>';
                                              }else{
                                                 echo '<span style="font-size: 10px" class="label label-danger arrowed">Inactivo</span>';
                                              }?>
                                        </td>
                                        <td class="center">
                                            <a href="#" class="blue"><i class="ace-icon fa fa-pencil bigger-150" data-toggle="modal" data-target="#edit_<?php echo $presentacion[0]; ?>" title="Editar"></i></a></button><span class="vbar"></span>
                                            <?php if ($presentacion[4]==0) { $estado="disabled";}else{$estado="";} ?>
                                            <a href="#" class="red <?php echo $estado ?>"><i class="ace-icon fa fa-trash-o bigger-150" data-toggle="modal" data-target="#delete_<?php echo $presentacion[0]; ?>" title="Editar cliente"></i></a><span class="vbar"></span>
                                        </td>
                                            <?php include('../modal/editarPresentacion.php'); ?>    
                                    </tr><?php
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
<script src="../assets/js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript">
            if('ontouchstart' in document.documentElement) document.write("<script src='../assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
        </script>
        <script src="../assets/js/bootstrap.min.js"></script>

        <!-- page specific plugin scripts -->
        <script src="../assets/js/jquery.dataTables.min.js"></script>
        <script src="../assets/js/jquery.dataTables.bootstrap.min.js"></script>
        <script src="../assets/js/dataTables.buttons.min.js"></script>
        <script src="../assets/js/buttons.flash.min.js"></script>
        <script src="../assets/js/buttons.html5.min.js"></script>
        <script src="../assets/js/buttons.print.min.js"></script>
        <script src="../assets/js/buttons.colVis.min.js"></script>
        <script src="../assets/js/dataTables.select.min.js"></script>

        <!-- ace scripts -->
        <script src="../assets/js/ace-elements.min.js"></script>
        <script src="../assets/js/ace.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#t_client').DataTable({
            "info": false,
            "language": {
            "url": "../assets/js/Spanish.json"
            }
        });
    });
</script>
</body>
</html>
<?php
require 'footer.html';
?>
