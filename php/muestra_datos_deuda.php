<?php
/*error_reporting(E_ALL);
ini_set('display_errors', '1');*/
session_start();
require_once 'conexion.php';
date_default_timezone_set('America/Lima');
$nomb=$_SESSION['nom'];
$valida=mysqli_num_rows(mysqli_query($conex,"SELECT Num_doc FROM usuario where Num_doc='$nomb'"));
if ($nomb== null or $nomb=="" or $valida==0) {
  header("location:../index.html");
}
if (isset($_POST['f_inicio'])) {
    $f_ini=$_POST['f_inicio'];
}else{
    $f_ini=date('Y-m-d');
}
if (isset($_POST['f_fin'])) {
    $f_final=$_POST['f_fin'];
}else{
    $f_final=date('Y-m-d');
}
$usuario=mysqli_fetch_row(mysqli_query($conex,"SELECT * FROM usuario WHERE Num_doc='$nomb'"));
$rs_ventas=mysqli_query($conex,"SELECT * FROM deudas a inner join venta b on a.Id_vta=b.Id_vta WHERE a.Fecha between '$f_ini' AND '$f_final'");
?>
<!DOCTYPE html>
<html>
<head>
    <title></title>
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="../assets/font-awesome/4.5.0/css/font-awesome.min.css" />
    <!-- text fonts -->
    <link rel="stylesheet" href="../assets/css/fonts.googleapis.com.css" />
    <!-- ace styles -->
    <link rel="stylesheet" href="../assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />
    <link rel="stylesheet" href="../assets/css/ace-skins.min.css" />
    <link rel="stylesheet" href="../assets/css/ace-rtl.min.css" />
    <!-- ace settings handler -->
    <script src="../assets/js/ace-extra.min.js"></script>
</head>
<body>
  <div class="main-content">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> Reporte de Deudas</h3>
            </div>
            <div class="table-responsive">
                <table id="t_client" class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr align="center">
                            <th>IT</th>
                            <th>Fecha</th>
                            <th>Cliente</th>
                            <th>Comprobante</th>
                            <th>Total Deuda</th>
                            <th>Estado</th>
                            <th>Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php $i=1; $total=0;
                    while ($rpte=mysqli_fetch_row($rs_ventas)) {
                      $cliente=mysqli_fetch_row(mysqli_query($conex,"SELECT * FROM clientes WHERE Id_client=$rpte[1]"));
                      $precio=$rpte[6];
                      $estado=$rpte[7];
                      $subtotal=$precio;
                      if ($estado==1) {
                          $total += $subtotal;
                      }
                      ?>
                        <tr align="center" <?php if ($estado==0) { echo 'class="danger"';} ?>>
                            <td><?php echo $i; ?></td>
                            <td><?php echo $rpte[3]; ?></td>
                            <td><?php echo $cliente[2]; ?></td>
                            <td><?php echo $rpte[12]; ?></td>
                            <td><?php echo $rpte[6]; ?></td>
                            <td><?php
                              if ($estado==0) {
                                echo '<span style="font-size: 10px;" class="label label-success arrowed-in arrowed-in-right">Pagado</span>';
                                $anulado='';
                              }else{
                                 echo '<span style="font-size: 10px" class="label label-danger arrowed">Pendiente</span>';
                                 $anulado='disabled="disabled"';
                              }?>
                            </td>
                            <td><center><a href="anular_venta.php?anula=<?php echo $rpte[0];?>" class="red" <?php echo $anulado;?> title="Anular Ticket"><i class="ace-icon fa fa-trash-o bigger-180"></i></a>
                                <span class="vbar"></span><a target="_blank" href="print-ticket.php?ticket=<?php echo $rpte[4];?>" class="green" <?php echo $anulado; ?> title="Imprimir Ticket"><i class="ace-icon fa fa-print bigger-180"></i></a></center></td>
                        </tr><?php $i++;
                        }
                    ?>
                    </tbody>
                    <tr>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td align="right"><b>Total: </b></td>
                      <td align="center"><b><?php echo number_format($total,2); ?></b></td>
                      <td></td>
                      <td></td>
                    </tr>
                </table>
            </div>
        </div>
    </div><!-- /.col -->
  </div>
  <script src="../assets/js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript">
            if('ontouchstart' in document.documentElement) document.write("<script src='../assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
        </script>
        <script src="../assets/js/bootstrap.min.js"></script>

        <!-- page specific plugin scripts -->
        <script src="../assets/js/jquery.dataTables.min.js"></script>
        <script src="../assets/js/jquery.dataTables.bootstrap.min.js"></script>
        <script src="../assets/js/dataTables.buttons.min.js"></script>
        <script src="../assets/js/buttons.flash.min.js"></script>
        <script src="../assets/js/buttons.html5.min.js"></script>
        <script src="../assets/js/buttons.print.min.js"></script>
        <script src="../assets/js/buttons.colVis.min.js"></script>
        <script src="../assets/js/dataTables.select.min.js"></script>

        <!-- ace scripts -->
        <script src="../assets/js/ace-elements.min.js"></script>
        <script src="../assets/js/ace.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#t_client').DataTable({
            "info": false,
            "language": {
            "url": "../assets/js/Spanish.json"
            }
        });
    });
</script>
</body>
</html>