<?php
session_start();
error_reporting(0);
require_once('conexion.php');
require 'header.php';
$nomb=$_SESSION['nom'];
$valida=mysqli_num_rows(mysqli_query($conex,"SELECT Num_doc FROM usuario where Num_doc='$nomb'"));
if ($nomb== null or $nomb=="" or $valida==0) {
  header("location:../index.html");
}
date_default_timezone_set('America/Lima');
setlocale ( LC_TIME, 'spanish' );
setlocale(LC_ALL,"es_ES@euro","es_ES","esp");
$fecha = strftime( "%A %d de %B del %Y" );
$rs_ventas=mysqli_query($conex,"SELECT a.Id_vta,a.Id_user,a.Fecha,SUM(b.Precio_vta),SUM(c.p_compra) FROM venta a INNER JOIN det_venta b ON b.Id_vta=a.Id_vta INNER JOIN articulo c ON b.Id_art=c.Id_art group by a.Id_user,a.Fecha ORDER BY a.Fecha");
#$rs_ventas=mysqli_query($conex,"SELECT Id_user, SUM(Total_vta) as Venta, Fecha FROM venta GROUP BY Id_user,Fecha");
$rs_compras=mysqli_query($conex,"SELECT * FROM pagos_prov WHERE Estado=1");
$usuario=mysqli_fetch_row(mysqli_query($conex,"SELECT * FROM usuario WHERE Num_doc='$nomb' AND estado=1"));
?>
<div class="main-content-inner">
	<div class="page-content">
		<div class="row">				
			<div class="space-6"></div>
			<div class="col-sm-7">
				<h5 class="header smaller lighter blue"><b>REPORTE GENERAL DE VENTAS</b></h5>
				<div class="table-responsive">
					<form action="#" method="POST">
						<!-- <input type="date" name="venta" min="2019-12-02" value="<?php echo date('Y-m-d'); ?>">
						<button type="submit" class="btn btn-white btn-success btn-sm btn-round"><span class="glyphicon glyphicon-search" title="Agregar al Carrito" aria-hidden="true"></span></button> -->
						<table id="Myorder" class="table table-striped table-bordered table-hover">
							<thead>
								<tr>
									<td>FECHA</td>
									<td>USUARIO</td>
									<td>TOTAL VENTA</td>
								</tr>
							</thead>
							<tbody><?php $total=0;$gana=0;
								while ($caja=mysqli_fetch_row($rs_ventas)) {
									$usuario=mysqli_fetch_row(mysqli_query($conex,"SELECT*FROM usuario WHERE Id_user=$caja[1]"));
									$total=$total+$caja[3];
									$gana=$gana+$caja[4];?>
									<tr>
										<td><?php echo $caja[2]; ?></td>
										<td><?php echo $usuario[1]; ?></td>
										<td align="center"><?php echo $caja[3]; ?></td>
									</tr><?php
								}
							 ?>
							</tbody>
							<tr>
								<td></td>
								<td align="right"><b>Total: </b></td>
              	<td align="center"><b><?php echo number_format($total,2); ?></b></td>
							</tr>
						</table>
					</form>
				</div>
			</div>
			<div class="vspace-12-sm"></div>
			<div class="col-sm-5"><?php
			$rs_top=mysqli_query($conex,"SELECT Id_det_vta,Id_vta,Id_art,count(Cant)as Toto,sum(precio_vta) FROM `det_venta` group by Id_art ORDER BY sum(precio_vta) DESC limit 10")
			 ?>
				<h5 class="header smaller lighter blue"><b>TOP 10 - SOLES</b></h5>
				<table class="table table-striped table-bordered table-hover">
					<thead>
						<th>IT</th>
						<th>PRODUCTOS</th>
						<th>CANT</th>
						<th>VENTA</th>
					</thead>
					<tbody>
						<?php $i=1;
						while ($top=mysqli_fetch_row($rs_top)) {
							$rs_art=mysqli_fetch_row(mysqli_query($conex,"SELECT Nombre,p_compra,(((precio/p_compra)*100)-100) FROM articulo WHERE Id_art=$top[2]"));?>
						<tr>
							<td><?php echo $i ?></td>
							<td><?php echo $rs_art[0] ?></td>
							<td><?php echo $top[3] ?></td>
							<td><?php echo $top[4] ?></td>
						</tr><?php $i++;
						}
						 ?>
					</tbody>
				</table>
			</div><!-- /.col -->
		</div><!-- /.row -->
		<div class="row"> <!--Acumulado ventas-->
			<div class="space-6"></div>
			<div class="col-sm-7">
				<h5 class="header smaller lighter blue"><b>REPORTE COMPRAS</b></h5>
				<div class="table-responsive">
					<table id="Myorder" class="table table-striped table-bordered table-hover">
							<thead>
								<tr>
									<td>FECHA</td>
									<td>PROVEEDOR</td>
									<td>BOLETA</td>
									<td>TOTAL</td>
								</tr>
							</thead>
							<tbody><?php $total=0;$gana=0;
								while ($caja=mysqli_fetch_row($rs_compras)) {
									$prov=mysqli_fetch_row(mysqli_query($conex,"SELECT*FROM proveedor WHERE Id_prov=$caja[1]"));
									$usuario=mysqli_fetch_row(mysqli_query($conex,"SELECT*FROM usuario WHERE Id_user=$caja[1]"));
									$total=$total+$caja[4];?>
									<tr>
										<td><?php echo $caja[2]; ?></td>
										<td><?php echo $prov[2]; ?></td>
										<td><?php echo $caja[5]; ?></td>
										<td align="center"><?php echo number_format($caja[4],2); ?></td>
									</tr><?php
								}
							 ?>
							</tbody>
							<tr>
								<td></td>
								<td></td>	
								<td align="right"><b>Total: </b></td>
              	<td align="center"><b><?php echo number_format($total,2); ?></b></td>
							</tr>
						</table>
				</div>
			</div>
			<div class="vspace-12-sm"></div>
			<div class="col-sm-5"><?php
			$rs_topu=mysqli_query($conex,"SELECT Id_det_vta,Id_vta,Id_art,count(Cant)as Toto,sum(precio_vta) FROM `det_venta` WHERE Id_art!=44 group by Id_art ORDER BY count(Cant) DESC limit 10")
			 ?>
				<h5 class="header smaller lighter blue"><b>TOP 10 - UNIDADES</b></h5>
				<table class="table table-striped table-bordered table-hover">
					<thead>
						<th>IT</th>
						<th>PRODUCTOS</th>
						<th>CANTIDAD</th>
						<th>PRECIO</th>
						<th>STOCK</th>
					</thead>
					<tbody>
						<?php $i=1;
						while ($topu=mysqli_fetch_row($rs_topu)) {
							$rs_arti=mysqli_fetch_row(mysqli_query($conex,"SELECT Nombre FROM articulo WHERE Id_art=$topu[2] AND Id_art!=44"));
							$stock=mysqli_fetch_row(mysqli_query($conex,"SELECT SUM(Stock) FROM stock WHERE Id_art=$topu[2]"));?>
						<tr>
							<td><?php echo $i ?></td>
							<td><?php echo $rs_arti[0] ?></td>
							<td><?php echo $topu[3] ?></td>
							<td><?php echo $topu[4] ?></td>
							<td><?php echo $stock[0] ?></td>
						</tr><?php $i++;
						}
						 ?>
					</tbody>
				</table>
			</div><!-- /.col -->
		</div>
	</div>
</div>
		<script src="../assets/js/jquery-2.1.4.min.js"></script>
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script src="../assets/js/bootstrap.min.js"></script>

		<!-- page specific plugin scripts -->
		<script src="../assets/js/jquery.dataTables.min.js"></script>
		<script src="../assets/js/jquery.dataTables.bootstrap.min.js"></script>
		<script src="../assets/js/dataTables.buttons.min.js"></script>
		<script src="../assets/js/buttons.flash.min.js"></script>
		<script src="../assets/js/buttons.html5.min.js"></script>
		<script src="../assets/js/buttons.print.min.js"></script>
		<script src="../assets/js/buttons.colVis.min.js"></script>
		<script src="../assets/js/dataTables.select.min.js"></script>

		<!-- ace scripts -->
		<script src="../assets/js/ace-elements.min.js"></script>
		<script src="../assets/js/ace.min.js"></script>
		<script type="text/javascript">
	      $(document).ready(function() {
	          $('#Myorder').DataTable({
	              "info": false,
	              "language": {
	              "url": "../assets/js/Spanish.json"
							},
							"order": [[ 0, "desc" ]],
	          });
	      });
	  	</script>
		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {
				$('.easy-pie-chart.percentage').each(function(){
					var $box = $(this).closest('.infobox');
					var barColor = $(this).data('color') || (!$box.hasClass('infobox-dark') ? $box.css('color') : 'rgba(255,255,255,0.95)');
					var trackColor = barColor == 'rgba(255,255,255,0.95)' ? 'rgba(255,255,255,0.25)' : '#E2E2E2';
					var size = parseInt($(this).data('size')) || 50;
					$(this).easyPieChart({
						barColor: barColor,
						trackColor: trackColor,
						scaleColor: false,
						lineCap: 'butt',
						lineWidth: parseInt(size/10),
						animate: ace.vars['old_ie'] ? false : 1000,
						size: size
					});
				})

				$('.sparkline').each(function(){
					var $box = $(this).closest('.infobox');
					var barColor = !$box.hasClass('infobox-dark') ? $box.css('color') : '#FFF';
					$(this).sparkline('html',
									 {
										tagValuesAttribute:'data-values',
										type: 'bar',
										barColor: barColor ,
										chartRangeMin:$(this).data('min') || 0
									 });
				});


			  //flot chart resize plugin, somehow manipulates default browser resize event to optimize it!
			  //but sometimes it brings up errors with normal resize event handlers
			  $.resize.throttleWindow = false;

			  var placeholder = $('#piechart-placeholder').css({'width':'90%' , 'min-height':'150px'});
			  var data = [
				{ label: "social networks",  data: 38.7, color: "#68BC31"},
				{ label: "search engines",  data: 24.5, color: "#2091CF"},
				{ label: "ad campaigns",  data: 8.2, color: "#AF4E96"},
				{ label: "direct traffic",  data: 18.6, color: "#DA5430"},
				{ label: "other",  data: 10, color: "#FEE074"}
			  ]
			  function drawPieChart(placeholder, data, position) {
			 	  $.plot(placeholder, data, {
					series: {
						pie: {
							show: true,
							tilt:0.8,
							highlight: {
								opacity: 0.25
							},
							stroke: {
								color: '#fff',
								width: 2
							},
							startAngle: 2
						}
					},
					legend: {
						show: true,
						position: position || "ne",
						labelBoxBorderColor: null,
						margin:[-30,15]
					}
					,
					grid: {
						hoverable: true,
						clickable: true
					}
				 })
			 }
			 drawPieChart(placeholder, data);

			 /**
			 we saved the drawing function and the data to redraw with different position later when switching to RTL mode dynamically
			 so that's not needed actually.
			 */
			 placeholder.data('chart', data);
			 placeholder.data('draw', drawPieChart);


			  //pie chart tooltip example
			  var $tooltip = $("<div class='tooltip top in'><div class='tooltip-inner'></div></div>").hide().appendTo('body');
			  var previousPoint = null;

			  placeholder.on('plothover', function (event, pos, item) {
				if(item) {
					if (previousPoint != item.seriesIndex) {
						previousPoint = item.seriesIndex;
						var tip = item.series['label'] + " : " + item.series['percent']+'%';
						$tooltip.show().children(0).text(tip);
					}
					$tooltip.css({top:pos.pageY + 10, left:pos.pageX + 10});
				} else {
					$tooltip.hide();
					previousPoint = null;
				}

			 });

				/////////////////////////////////////
				$(document).one('ajaxloadstart.page', function(e) {
					$tooltip.remove();
				});




				var d1 = [];
				for (var i = 0; i < Math.PI * 2; i += 0.5) {
					d1.push([i, Math.sin(i)]);
				}

				var d2 = [];
				for (var i = 0; i < Math.PI * 2; i += 0.5) {
					d2.push([i, Math.cos(i)]);
				}

				var d3 = [];
				for (var i = 0; i < Math.PI * 2; i += 0.2) {
					d3.push([i, Math.tan(i)]);
				}


				var sales_charts = $('#sales-charts').css({'width':'100%' , 'height':'220px'});
				$.plot("#sales-charts", [
					{ label: "Domains", data: d1 },
					{ label: "Hosting", data: d2 },
					{ label: "Services", data: d3 }
				], {
					hoverable: true,
					shadowSize: 0,
					series: {
						lines: { show: true },
						points: { show: true }
					},
					xaxis: {
						tickLength: 0
					},
					yaxis: {
						ticks: 10,
						min: -2,
						max: 2,
						tickDecimals: 3
					},
					grid: {
						backgroundColor: { colors: [ "#fff", "#fff" ] },
						borderWidth: 1,
						borderColor:'#555'
					}
				});


				$('#recent-box [data-rel="tooltip"]').tooltip({placement: tooltip_placement});
				function tooltip_placement(context, source) {
					var $source = $(source);
					var $parent = $source.closest('.tab-content')
					var off1 = $parent.offset();
					var w1 = $parent.width();

					var off2 = $source.offset();
					//var w2 = $source.width();

					if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
					return 'left';
				}


				$('.dialogs,.comments').ace_scroll({
					size: 300
			    });


				//Android's default browser somehow is confused when tapping on label which will lead to dragging the task
				//so disable dragging when clicking on label
				var agent = navigator.userAgent.toLowerCase();
				if(ace.vars['touch'] && ace.vars['android']) {
				  $('#tasks').on('touchstart', function(e){
					var li = $(e.target).closest('#tasks li');
					if(li.length == 0)return;
					var label = li.find('label.inline').get(0);
					if(label == e.target || $.contains(label, e.target)) e.stopImmediatePropagation() ;
				  });
				}

				$('#tasks').sortable({
					opacity:0.8,
					revert:true,
					forceHelperSize:true,
					placeholder: 'draggable-placeholder',
					forcePlaceholderSize:true,
					tolerance:'pointer',
					stop: function( event, ui ) {
						//just for Chrome!!!! so that dropdowns on items don't appear below other items after being moved
						$(ui.item).css('z-index', 'auto');
					}
					}
				);
				$('#tasks').disableSelection();
				$('#tasks input:checkbox').removeAttr('checked').on('click', function(){
					if(this.checked) $(this).closest('li').addClass('selected');
					else $(this).closest('li').removeClass('selected');
				});


				//show the dropdowns on top or bottom depending on window height and menu position
				$('#task-tab .dropdown-hover').on('mouseenter', function(e) {
					var offset = $(this).offset();

					var $w = $(window)
					if (offset.top > $w.scrollTop() + $w.innerHeight() - 100)
						$(this).addClass('dropup');
					else $(this).removeClass('dropup');
				});

			})
		</script>
		<?php
			require 'footer.html';
		?>

	</body>
</html>
