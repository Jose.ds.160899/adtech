<?php
session_start();
require_once('conexion.php');
date_default_timezone_set('America/Lima');
$home_menu=1;
$nomb=$_SESSION['nom'];
$permiso="Ventas";
$rs_user=mysqli_fetch_row(mysqli_query($conex,"SELECT Id_user FROM usuario where Num_doc='$nomb'"));
$id_user=$rs_user[0];
$sql = mysqli_query($conex, "SELECT p.*, d.* FROM permiso p INNER JOIN usuario_permiso d ON p.idpermiso = d.idpermiso WHERE d.id_user = $id_user AND p.nombre = '$permiso'");
$existe = mysqli_fetch_all($sql);
if (empty($existe) && $id_user != 1) {
    header("Location: permisos.php");
}
include 'header.php';
include '../modal/nuevo_venta.php';
$rs_inventario=mysqli_query($conex,"SELECT a.Nombre,a.Composicion,a.p_compra,c.Nom_tipo_art,a.Fabricante,a.Precio,b.Stock,b.Fecha_Venc,b.id_stock,a.id_art FROM articulo a INNER JOIN Stock b ON a.Id_art = b.Id_art INNER JOIN tipo_articulo c ON a.Id_tipo_art=c.Id_tipo_art AND b.stock>0");
$rs_cant=mysqli_num_rows($rs_inventario);
$rs_carrito=mysqli_query($conex,"SELECT * FROM carrito,articulo,stock WHERE carrito.Id_art=articulo.Id_art AND carrito.Id_stock=stock.Id_stock AND carrito.Id_user = '$id_user'");
/*$rs_Ticket=mysqli_fetch_row(mysqli_query($conex,"SELECT CONCAT('T001-',LPAD(CONVERT(RIGHT(Num_comp,6)+1,CHAR), 6,'0')) AS Serie FROM correlativo WHERE Tipo_comp='Ticket'"));*/
$rs_cliente=mysqli_query($conex,"SELECT * FROM clientes");
$rs_correlativo=mysqli_query($conex,"SELECT * FROM Correlativo WHERE Tipo_doc='Comprobante'");
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <title></title>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css" />
        <link rel="stylesheet" href="../assets/font-awesome/4.5.0/css/font-awesome.min.css" />
        <!-- text fonts -->
        <link rel="stylesheet" href="../assets/css/fonts.googleapis.com.css" />
        <!-- ace styles -->
        <link rel="stylesheet" href="../assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />
        <link rel="stylesheet" href="../assets/css/ace-skins.min.css" />
        <link rel="stylesheet" href="../assets/css/ace-rtl.min.css" />
        <link rel="stylesheet" href="../assets/css/chosen.min.css" />
        <!-- ace settings handler -->
        <script src="../assets/js/ace-extra.min.js"></script>
       <style type="text/css">
         .marito{
          padding: 2px;
          margin: 0px;
          font-size: 11px;
          border-radius: 4px;
          font-family: 'Verdana';
         }
          input[type="number"]::-webkit-outer-spin-button,
          input[type="number"]::-webkit-inner-spin-button {
              -webkit-appearance: none;
              margin: 0;
          }
          input[type="number"] {
              -moz-appearance: textfield;
          }
          .panel-body{
            padding: 5px;
          }
          .text-center{
            text-align: center !important;
          }
          .info{
            text-align: center !important;
          }
          .d-none{
            display: none;
          }
       </style>
       
</head>
<body class="no-skin">
  
  <div class="main-content">
    <div class="main-content-inner">
      <div class="breadcrumbs ace-save-state" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon green fa fa-shopping-cart home-icon"></i>
                    <a href="#"><?php echo $usuario[7] ?></a>
                </li>
                <li class="active blue"><?php echo $usuario[1];?></li>
            </ul><!-- /.breadcrumb -->
      </div>
      <div class="page-content">
        <div class="row panel-body">
          <div class="col-md-4">
            <button type="button" class="btn btn-white btn-sm btn-success btn-round" data-toggle="modal" data-target="#modal-venta">Agregar Productos <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span></button>	
          </div>
        </div>
        <form method="POST" action="guardar_venta.php" id="formulario_ventas">
          <div class="row panel-body">
            <div class="col-md-4">
              <select class="chosen-select form-control" id="id_clie" name="id_clie" required>
                <option disabled selected>Seleccionar Cliente</option><?php
                  while ($cliente=mysqli_fetch_row($rs_cliente)) { ?>
                    <option value="<?php echo $cliente[0]?>" data-estado="<?= $cliente[10]; ?>"><?php echo $cliente[2]?></option>  
                  <?php } ?>
              </select>
            </div>
            <div class="col-md-2">
              <select class="chosen-select form-control" name="cboMoneda" id="cboMoneda" required>
                <option disabled selected>Seleccionar Moneda</option>
                <option value="SOL">SOLES</option>
                <option value="DOLAR">DOLARES</option>
              </select>
            </div>
            <div class="col-md-2">
              <select class="chosen-select form-control" name="corr" id="corr" required>
                <option disabled selected>Seleccionar documento</option><?php 
                while ($correlativo=mysqli_fetch_row($rs_correlativo)) { ?>
                   <option value="<?php echo $correlativo[0] ?>"><?php echo $correlativo[1] ?></option>
                 <?php } ?>
              </select>
            </div>
            
            <div class="col-md-2">
              <select class="form-control" id="dato" name="dato"> </select>
            </div>
            <div class="col-md-2">
                <select class="form-control" id="ncorr" name="ncorr"> </select>
            </div>
          </div>
          <div class="row">
            <div class="panel-body" id="order">

              <div class="col-md-12">
                <div class="panel panel-primary">
                  <div class="panel-heading">
                      <h3 class="panel-title"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> Carrito de Compras</h3>
                  </div>
                  <div class="table-responsive">
                    
                      <table id="Mycart" class="table table-striped table-bordered table-hover text-center">
                        <thead>
                          <tr class="info">
                            <th width="35" class="text-center">Producto</th>
                            <th width="25" class="text-center">Precio Uni.</th>
                            <th width="10" class="text-center">Cantidad</th>
                            <th width="10" class="text-center">SubTotal</th>
                            <th width="10" class="text-center">Acciones</th>
                          </tr>
                        </thead>
                        <tbody class="text-center" id="tbody-venta">
                          <?php
                            $total = 0;
                            $rows=mysqli_num_rows($rs_carrito);
                          
                            while ($carrito=mysqli_fetch_row($rs_carrito)) {
                              $precio=number_format($carrito[3],2);
                              $cant = $carrito[2];
                              $subtotal= ($precio) * ($cant);
                              $total += $subtotal;
                          ?>
                          <tr align="center">
                            <td><?php echo $carrito[10]; ?></td>
                            <td>
                              <input style="width: 50%;text-align: center; font-size: 12px;" type="number"  value="<?php echo number_format($carrito[3],2) ?>" class="input-cantidad" data-cantidad="<?php echo $cant; ?>" data-id="<?php echo $carrito[0]; ?>">  
                            </td>
                            <td><?php echo $carrito[2]; ?></td>
                            <td class="td-<?php echo $carrito[0];?>">
                              <input type="number" style="width: 50%;text-align: center; font-size: 12px;" class="input-subtotal-<?php echo $carrito[0];?>" value="<?php echo number_format($subtotal,2); ?>" disabled>
                            </td>
                            <td align="center">
                              <button type="button" class="btn btn-white btn-danger btn-xs btn-round" ><a class="text-danger" href="guardar_carrito.php?op=1&idcart=<?php echo $carrito[0];?>"><span class="glyphicon glyphicon-trash" title="Eliminar Item" aria-hidden="true"></span></a></button>
                            </td>
                          </tr><?php } ?>
                        </tbody>
                        <tr>
                          <td style="display: flex; justify-content: center; align-content: center;">
                            <strong style="font-size: 11px;">Forma de Pago:</strong>
                            <select style="font-size: 11px;" class="form-control" name="tipo_vta" id="tipo_vta">
                              <option value="CONT">CONTADO</option>
                              <option value="CRED" class="tipo_credito" disabled>CREDITO</option>
                              <option value="DEPO">DEPOSITO</option>
                             <!--  <option value="YAPE">YAPE</option>
                              <option value="PLIN">PLIN</option> -->
                            </select>
                          </td>
                          <td></td>
                          <!-- <td></td> -->
                          <!-- <td></td> -->
                          <td align="right"><strong>Total:</strong></td>
                          <td align="center">
                              <strong class="total-td"><?php echo number_format($total, 2); ?></strong>
                          </td>

                          <td align="center">
                            <div class="<?= ($total > 0) ? '':'d-none' ?> td-mostrar-boton-vender">
                              <input type="hidden" name="row" value="<?php echo $rows;?>">
                              <input type="hidden" name="corre" value="<?php echo $rs_Ticket[0];?>">
                              <input type="hidden" name="tot_vta" value="<?php echo number_format($total,2);?>">
                              <button type="submit" class="btn btn-white btn-success btn-xs btn-round">Vender<span style="padding: 0px" class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span></button>
                              <!-- <button type="submit" class="btn btn-white btn-danger btn-xs btn-round"><a class="text-danger" href="guardar_venta.php?op=1"><span style="padding: 0px" class="glyphicon glyphicon-remove" aria-hidden="true"></span></a></button> -->
                            </div>
                          </td>
                        </tr>
                      </table>
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div><!-- /.main-content -->
  <script src="../assets/js/jquery-2.1.4.min.js"></script>
  <script type="text/javascript">
      if('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
    </script>
  <script src="../assets/js/bootstrap.min.js"></script>
  <script src="../assets/js/chosen.jquery.min.js"></script>

  <!-- page specific plugin scripts -->
  <script src="../assets/js/jquery.dataTables.min.js"></script>
  <script src="../assets/js/jquery.dataTables.bootstrap.min.js"></script>
  <script src="../assets/js/dataTables.buttons.min.js"></script>
  <script src="../assets/js/buttons.flash.min.js"></script>
  <script src="../assets/js/buttons.html5.min.js"></script>
  <script src="../assets/js/buttons.print.min.js"></script>
  <script src="../assets/js/buttons.colVis.min.js"></script>
  <script src="../assets/js/dataTables.select.min.js"></script>
  <script src="../assets/js/chosen.jquery.min.js"></script>

  <!-- ace scripts -->
  <script src="../assets/js/ace-elements.min.js"></script>
  <script src="../assets/js/ace.min.js"></script>

  <script>
    function agregar(id)
    {
      var cant=document.getElementById('cant'+id).value;
      var id_art=document.getElementById('id_art'+id).value;
      var id_stock=document.getElementById('id_stock'+id).value;
      var id_user=document.getElementById('id_user'+id).value;
      var precio_art=document.getElementById('precio_art'+id).value;
      var stock=document.getElementById('stock'+id).value;
      //validacion
      var valida = stock - cant;
        if(valida < 0){
          alert('No hay stock para vender esa cantidad!');
        }else{
          var parametros = {
            "cant" : cant,
            "id_art" : id_art,
            "id_stock" : id_stock,
            "id_user" : id_user,
            "precio_art" : precio_art
          };
          $.ajax({
            data:  parametros, //datos que se envian a traves de ajax
            url:   'guardar_carrito.php', //archivo que recibe la peticion
            type:  'post', //método de envio
            /*beforeSend: function () {
                    $("#Mycart").html("Procesando...");
            },*/
            /*success:  function (response) { //una vez que el archivo recibe el request lo procesa y lo devuelve
              $("#Mycart").html(response);
            }*/
            success: function (data) {
              console.log(data);
              showOrder();
            }
          });
        }
    }

    function showOrder(){
      $.ajax({
        url:'listarCarritoVentas.php',
        type: 'GET',
        success: function(e){
          let rpta = JSON.parse(e);
          console.log(rpta);

          if(!$.isEmptyObject(rpta)){
            html_c = '';
            rpta.forEach((e,i)=>{

              Precio_Art = parseFloat(e.Precio_art).toFixed(2);
              subtotal = (parseFloat(e.Precio_art) * parseFloat(e.Cant_cart)).toFixed(2);
              console.log(Precio_Art);
              console.log(subtotal);

              html_c += `<tr align="center">
                      <td>${e.Nombre}</td>
                      <td>
                        <input style="width: 50%;text-align: center; font-size: 12px;" type="number"  value="${Precio_Art}" class="input-cantidad" data-cantidad="${e.Cant_cart}" data-id="${e.Id_cart}">  
                      </td>
                      <td>${e.Cant_cart}</td>
                      <td class="td-${e.Id_cart}">
                        <input type="number" style="width: 50%;text-align: center; font-size: 12px;" class="input-subtotal-${e.Id_cart}" value="${subtotal}" disabled>
                      </td>
                      <td align="center">
                        <button type="button" class="btn btn-white btn-danger btn-xs btn-round" ><a class="text-danger" href="guardar_carrito.php?op=1&idcart=${e.Id_cart}"><span class="glyphicon glyphicon-trash" title="Eliminar Item" aria-hidden="true"></span></a></button>
                      </td>
                    </tr>`;
            });


            $("#tbody-venta").html(html_c);

            sumarTotal1();
          }
        }
      })
    }

    function sumarTotal1(){
      total = 0;
      $("#tbody-venta").children().each(function (index, element) {
          subtotal = parseFloat(element.cells[3].childNodes[1].value);
          total += subtotal;
      });
      if(total > 0){
        $(".td-mostrar-boton-vender").removeClass('d-none');
      }else{
        $(".td-mostrar-boton-vender").addClass('d-none');
      }
      $(".total-td").text(parseFloat(total).toFixed(2));
      $("#tot_vta").val(parseFloat(total).toFixed(2));
    }
    // function showOrder(){    
    //     $.ajax({
    //       url: 'order.php',
    //       type: 'post',
    //       success: function (data) {
    //         $('#order').html(data);
    //       },
    //       error: function(){
    //         eMsg('297');
    //       }
    //     });
    // }
  </script>
  <script type="text/javascript">
      $(document).ready(function() {
          $('#Myorder').DataTable({
              "iDisplayLength": 10,
              "info": false,
              "language": {
              "url": "../assets/js/Spanish.json"
              }
          });


          $("#id_clie").change(function(){
              estado = $("#id_clie option:selected").attr('data-estado');
              if(estado == 3){
                $(".tipo_credito").attr('disabled',false);
              }else{
                $(".tipo_credito").attr('disabled',true);
                $("#tipo_vta").val("CONT");
              }
          });

          $("#order").on('keyup','.input-cantidad',function(e){
              console.log(e.which);
              id = $(this).attr('data-id');
              valor = parseFloat($(this).val());
              cantidad = parseFloat($(this).attr('data-cantidad'));
              if(e.which != 8){
                // alert("aqui");
                $.ajax({
                  url: "guardar_descuento.php",
                  type: "GET",
                  data: {"id_carrito": id,"precio": valor},
                  success: function(e){
                    console.log("CORRECTO");
                    sub = (valor * cantidad);
                    $(".input-subtotal-"+id).val(parseFloat(sub).toFixed(2));
                    sumarTotal();
                  }
                })
              }
          });


          $("#formulario_ventas").submit(function(){
              event.preventDefault();
              // console.log($());
              if($("#id_clie").val()!= null && $("#cboMoneda").val()!=null && $("#corr").val()!=null && $("#dato").val() != null && $("#ncorr").val() != null){
                event.currentTarget.submit();
              }else{
                alert("TODOS LOS CAMPOS DEL FORMULARIO SON REQUERIDOS: (Cliente, Moneda,Documento,Serie, Numero)");
              }
          });

          $("#corr").change(function () {
            $("#corr option:selected").each(function () {
              elegido=$(this).val();
              $.post("busca_documento.php", { elegido: elegido }, function(data){
                $("#dato").html(data);
              });
            });
          })
          $("#dato").change(function () {
            $("#dato option:selected").each(function () {      
              elegido1=$(this).val();
              $.post("busca_correlativo.php", { elegido1: elegido1 }, function(data){
                $("#ncorr").html(data);
              });     
            });
          })   
          function sumarTotal(){
            total = 0;
            $("#tbody-venta").children().each(function (index, element) {
                subtotal = parseFloat(element.cells[3].childNodes[1].value);
                total += subtotal;
            });
            $(".total-td").text(parseFloat(total).toFixed(2));
            $("#tot_vta").val(parseFloat(total).toFixed(2));
          }


          
      });
  </script>
 <!--  <script type="text/javascript">
    $(document).ready(function() {
        $('#Mycar').DataTable({
            "info": false,
            "language": {
            "url": "../assets/js/Spanish.json"
            },
            "searching": false,

        });
    });
    var mostrarValor = function(x){
      document.getElementById('correl').value=x;
    }
  </script> -->
  <script>
  	jQuery(function($) {
    	if(!ace.vars['touch']) {
  			$('.chosen-select').chosen({allow_single_deselect:true,width: '100%'});
  			//resize the chosen on window resize
  		}
      $('#task-tab .dropdown-hover').on('mouseenter', function(e) {
          var offset = $(this).offset();

          var $w = $(window)
          if (offset.top > $w.scrollTop() + $w.innerHeight() - 100)
            $(this).addClass('dropup');
          else $(this).removeClass('dropup');
        });
    });
  </script>
</body>
</html>
<?php
require 'footer.html';
?>
