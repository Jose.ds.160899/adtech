<?php
session_start();
require_once('conexion.php');
date_default_timezone_set('America/Lima');
$nomb=$_SESSION['nom'];
$id_detalle=$_GET['id_detalle'];
$id_cliente=$_GET['id_clie'];
$comprobante=$_GET['comprobante'];
$valida=mysqli_num_rows(mysqli_query($conex,"SELECT Num_doc FROM usuario where Num_doc='$nomb'"));
if ($nomb== null or $nomb=="" or $valida==0) {
  header("location:index.html");
}
$rs_detalle=mysqli_query($conex,"SELECT * FROM det_venta WHERE Id_vta=$id_detalle");

?>
<!DOCTYPE html>
<html>
<head>
    <title></title>
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="../assets/font-awesome/4.5.0/css/font-awesome.min.css" />
    <!-- text fonts -->
    <link rel="stylesheet" href="../assets/css/fonts.googleapis.com.css" />
    <!-- ace styles -->
    <link rel="stylesheet" href="../assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />
    <link rel="stylesheet" href="../assets/css/ace-skins.min.css" />
    <link rel="stylesheet" href="../assets/css/ace-rtl.min.css" />
    <!-- ace settings handler -->
    <script src="../assets/js/ace-extra.min.js"></script>
    <style type="text/css">
        .disabled {
            cursor: not-allowed;
            pointer-events: none;
        }
    </style>
</head>
<body>
<div class="main-content">
    <div class="main-content-inner">
        <div class="page-content">
            <div class="row">
                <div>
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><span class="glyphicon glyphicon-list" aria-hidden="true"></span> <?php echo $id_cliente.' - '.$comprobante ?></h3>
                        </div>
                            <div class="table-responsive">
                                <table id="t_detalle" class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr class="info">
                                            <th>RUC</th>
                                            <th>PRODUCTO</th>
                                            <th>CANTIDAD</th>
                                            <th>PRECIO</th>
                                            <th>ESTADO</th>
                                        </tr>
                                    </thead>
                                    <tbody><?php $x=1;
                                    while ($detalle_venta=mysqli_fetch_row($rs_detalle)) { 
                                        $producto=mysqli_fetch_row(mysqli_query($conex,"SELECT * FROM articulo WHERE Id_art=$detalle_venta[2]"));
                                        $nom_art=$producto[4] ?>
                                        <tr>
                                            <td><?php echo $x?></td>
                                            <td><?php echo $nom_art?></td>
                                            <td><?php echo $detalle_venta[3]?></td>
                                            <td><?php echo $detalle_venta[4]?></td>
                                            <td class="center"><?php
                                              if ($detalle_venta[6]==1) {
                                                echo '<span style="font-size: 10px;" class="label label-success arrowed-in arrowed-in-right">Activo</span>';
                                              }else{
                                                 echo '<span style="font-size: 10px" class="label label-danger arrowed">Inactivo</span>';
                                              }?>
                                            </td>
                                        </tr><?php
                                    }?>
                                    </tbody>
                            </table>
                        </div>
                    </div>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
<script src="../assets/js/jquery-2.1.4.min.js"></script>
        <!-- <script type="text/javascript">
            if('ontouchstart' in document.documentElement) document.write("<script src='../assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
        </script> -->
        <script src="../assets/js/bootstrap.min.js"></script>

        <!-- page specific plugin scripts -->
        <script src="../assets/js/jquery.dataTables.min.js"></script>
        <script src="../assets/js/jquery.dataTables.bootstrap.min.js"></script>
        <script src="../assets/js/dataTables.buttons.min.js"></script>
        <script src="../assets/js/buttons.flash.min.js"></script>
        <script src="../assets/js/buttons.html5.min.js"></script>
        <script src="../assets/js/buttons.print.min.js"></script>
        <script src="../assets/js/jszip.min.js"></script>
        <script src="../assets/js/buttons.colVis.min.js"></script>
        <script src="../assets/js/dataTables.select.min.js"></script>

        <!-- ace scripts -->
        <script src="../assets/js/ace-elements.min.js"></script>
        <script src="../assets/js/ace.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#t_detalle').DataTable({
            "info": false,
            "language": {
            "url": "../assets/js/Spanish.json"
            },
            dom: 'Bfrtip',
            buttons: [
                {"extend": 'print', "text": '<span class = "glyphicon glyphicon-print"> </span>', "className": 'btn btn-success btn-xs'},
                {"extend": 'excel', "text": '<span class = "glyphicon glyphicon-list"></span>', "className": 'btn btn-success btn-xs'}
            ]
        });
    });
    $(document).ready(function() {
        $('#t_deuda').DataTable({
            "info": false,
            "language": {
            "url": "../assets/js/Spanish.json"
            }
        });
    });
    $('.show-details-btn').on('click', function(e) {
        e.preventDefault();
        $(this).closest('tr').next().toggleClass('open');
        $(this).find(ace.vars['.icon']).toggleClass('fa-angle-double-down').toggleClass('fa-angle-double-up');
    });

</script>
</body>
</html>
<?php
require 'footer.html';
?>
