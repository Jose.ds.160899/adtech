<?php
session_start();
require_once('conexion.php');
date_default_timezone_set('America/Lima');
$inventario_menu=1;
$nomb=$_SESSION['nom'];
/*$valida=mysqli_num_rows(mysqli_query($conex,"SELECT Num_doc FROM usuario where Num_doc='$nomb'"));
if ($nomb== null or $nomb=="" or $valida==0) {
  header("location:../index.html");
}*/
$permiso="Ingreso Inventario";
$rs_user=mysqli_fetch_row(mysqli_query($conex,"SELECT Id_user FROM usuario where Num_doc='$nomb'"));
$id_user=$rs_user[0];
$sql = mysqli_query($conex, "SELECT p.*, d.* FROM permiso p INNER JOIN usuario_permiso d ON p.idpermiso = d.idpermiso WHERE d.id_user = $id_user AND p.nombre = '$permiso'");
$existe = mysqli_fetch_all($sql);
if (empty($existe) && $id_user != 1) {
    header("Location: permisos.php");
}
require 'header.php';
include '../modal/nuevo_stock.php';
$rs_inventario=mysqli_query($conex,"SELECT * FROM articulo,stock,tipo_articulo WHERE articulo.Id_art = stock.Id_art AND articulo.Id_tipo_art=tipo_articulo.Id_tipo_art AND stock!=0");
?>
<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta charset="UTF-8"/>
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="../assets/font-awesome/4.5.0/css/font-awesome.min.css" />
    <!-- text fonts -->
    <link rel="stylesheet" href="../assets/css/fonts.googleapis.com.css" />
    <!-- ace styles -->
    <link rel="stylesheet" href="../assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />
    <link rel="stylesheet" href="../assets/css/ace-skins.min.css" />
    <link rel="stylesheet" href="../assets/css/ace-rtl.min.css" />
    <link rel="stylesheet" href="../assets/css/jquery-ui.custom.min.css" />
    <link rel="stylesheet" href="../assets/css/chosen.min.css" />
    <!-- ace settings handler -->
    <script src="../assets/js/ace-extra.min.js"></script>
    <style type="text/css">
        input[type="number"]::-webkit-outer-spin-button,
          input[type="number"]::-webkit-inner-spin-button {
              -webkit-appearance: none;
              margin: 0;
          }
          input[type="number"] {
              -moz-appearance: textfield;
              font-size: 11px;
              padding: 0px;
              width: 40%;
          }
    </style>
</head>
<body>
<div class="main-content">
    <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">Home</a>
                </li>
                <li class="active">Inventario</li>
            </ul><!-- /.breadcrumb -->
        </div>

        <div class="page-content">
            <div class="page-header">
                <button type="button" class="btn btn-white btn-sm btn-success btn-round" data-toggle="modal" data-target="#modal-stock">Agregar Inventario <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span></button>
            </div><!-- /.page-header -->
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><span class="glyphicon glyphicon-list" aria-hidden="true"></span> Inventario de Productos</h3>
                        </div>
                            <div class="table-responsive">
                                <table id="Myorder" class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr class="info">
                                            <th>Item</th>
                                            <th>Nombre</th>
                                            <th>F. Compra</th>
                                            <th>Vence</th>
                                            <th>Costo</th>
                                            <th>Precio</th>
                                            <th width="10%">Stock</th>
                                            <th>Opción</th>
                                        </tr>
                                    </thead>
                                    <tbody><?php $x=1;
                                    		$dateNow = date('Y-m');
                                        while ($inventario=mysqli_fetch_row($rs_inventario)) {
                                        	$xDate = date_create($inventario[14]);
                                        	$xDate = date_format($xDate,'Y-m');
						                    $class = 'text-success';
						                    if($xDate <= $dateNow){
						                        $class = 'danger';
						                    } ?>
                                        <tr align="center" class="<?php echo $class; ?>">
                                            <td><?php echo $x;?></td>
                                            <td align="left"><?php echo $inventario[4];?></td>
                                            <td><?php echo $inventario[13];?></td>
                                            <td><?php echo $inventario[14];?>
                                                <?php if($xDate <= $dateNow): ?>
                                                    <span class="label label-danger">!</span>
                                                <?php endif; ?></td>
                                            <td><?php echo $inventario[8];?></td>
                                            <td><?php echo $inventario[5];?></td>
                                            <form method="POST" action="actualiza_stock.php">
                                                <td align="center"><input type="number" name="cant" value="<?php echo $inventario[12];?>"></td>
                                                <td><button type="submit" class="btn btn-white btn-danger btn-xs btn-round" ><span class="glyphicon glyphicon-share" title="Actualizar Stock" aria-hidden="true"></span></button></td>
                                                <input type="hidden" name="id_stock" value="<?php echo $inventario[10];?>">
                                            </form>
                                        </tr><?php $x++; } ?>
                                    </tbody>
                                </table>
                            </div>
                    </div>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
<script src="../assets/js/jquery-2.1.4.min.js"></script>
<script src="../assets/js/chosen.jquery.min.js"></script>
<script type="text/javascript">
    if('ontouchstart' in document.documentElement) document.write("<script src='../assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
</script>
<script src="../assets/js/bootstrap.min.js"></script>

<!-- page specific plugin scripts -->
<script src="../assets/js/jquery.dataTables.min.js"></script>
<script src="../assets/js/jquery.dataTables.bootstrap.min.js"></script>
<script src="../assets/js/dataTables.buttons.min.js"></script>
<script src="../assets/js/buttons.flash.min.js"></script>
<script src="../assets/js/buttons.html5.min.js"></script>
<script src="../assets/js/buttons.print.min.js"></script>
<script src="../assets/js/buttons.colVis.min.js"></script>
<script src="../assets/js/dataTables.select.min.js"></script>

<!-- ace scripts -->
<script src="../assets/js/ace-elements.min.js"></script>
<script src="../assets/js/ace.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
         //$('#item-id').select2();
        $('#Myorder').DataTable({
            "info": false,
            "language": {
            "url": "../assets/js/Spanish.json"
            }
        });
    });

    jQuery(function($) {
    	if(!ace.vars['touch']) {
			$('.chosen-select').chosen({allow_single_deselect:true,width: '100%'});
			//resize the chosen on window resize

			$('#modal-stock').on('shown.bs.modal', function () {
                    if(!ace.vars['touch']) {
                        $(this).find('.chosen-container').each(function(){
                            $(this).find('a:first-child').css('width' , '100%');
                            $(this).find('.chosen-drop').css('width' , '100%');
                            $(this).find('.chosen-search input').css('width' , '100%');
                        });
                    }
                })
		}
    });
</script>
</body>
</html>



<?php
require 'footer.html';
?>
