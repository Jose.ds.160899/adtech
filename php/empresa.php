<?php
session_start();
require_once('conexion.php');
date_default_timezone_set('America/Lima');
$empresa_menu=1;
$nomb=$_SESSION['nom'];
$permiso="Empresa";
$rs_user=mysqli_fetch_row(mysqli_query($conex,"SELECT Id_user FROM usuario where Num_doc='$nomb'"));
$id_user=$rs_user[0];
$sql = mysqli_query($conex, "SELECT p.*, d.* FROM permiso p INNER JOIN usuario_permiso d ON p.idpermiso = d.idpermiso WHERE d.id_user = $id_user AND p.nombre = '$permiso'");
$existe = mysqli_fetch_all($sql);
if (empty($existe) && $id_user != 1) {
    header("Location: permisos.php");
}
require 'header.php';
include '../modal/nuevo_empresa.php';
$rs_empresa=mysqli_query($conex,"SELECT * FROM empresa");
?>
<!DOCTYPE html>
<html>
<head>
    <title></title>
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="../assets/font-awesome/4.5.0/css/font-awesome.min.css" />
    <!-- text fonts -->
    <link rel="stylesheet" href="../assets/css/fonts.googleapis.com.css" />
    <!-- ace styles -->
    <link rel="stylesheet" href="../assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />
    <link rel="stylesheet" href="../assets/css/ace-skins.min.css" />
    <link rel="stylesheet" href="../assets/css/ace-rtl.min.css" />
    <!-- ace settings handler -->
    <script src="../assets/js/ace-extra.min.js"></script>
</head>
<body>
    <div class="main-content">
    <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">Home</a>
                </li>
                <li class="active">Empresa</li>
            </ul><!-- /.breadcrumb -->
        </div>

        <div class="page-content">
            <div class="page-header">
                <button type="button" class="btn btn-white btn-sm btn-success btn-round" data-toggle="modal" data-target="#modal-empresa">Agregar Empresa <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span></button>

            </div><!-- /.page-header -->
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><span class="glyphicon glyphicon-list" aria-hidden="true"></span> Registro de Empresa</h3>
                        </div>
                        <div class="table-responsive">
                            <table id="t_client" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr class="info">
                                        <th>IT</th>
                                        <th>Nombre</th>
                                        <th>Documento</th>
                                        <th>Numero</th>
                                        <th>Dirección</th>
                                        <th>Telefono</th>
                                        <th>email</th>
                                        <th>logo</th>
                                        <th>OP</th>
                                    </tr>
                                </thead>
                                <tbody><?php
                                while ($empresa=mysqli_fetch_row($rs_empresa)) { ?>
                                    <tr>
                                        <td><?php echo $empresa[0]; ?></td>
                                        <td><?php echo $empresa[1]; ?></td>
                                        <td><?php echo $empresa[2]; ?></td>
                                        <td><?php echo $empresa[3]; ?></td>
                                        <td><?php echo $empresa[4]; ?></td>
                                        <td><?php echo $empresa[5]; ?></td>
                                        <td><?php echo $empresa[6]; ?></td>
                                        <td><?php echo $empresa[7]; ?></td>
                                        <td><center><a href="#" class="blue"><i class="ace-icon fa fa-pencil bigger-150" data-toggle="modal" data-target="#edit_<?php echo $empresa[0]; ?>" title="Editar"></i></a>
                                            <span class="vbar"></span>
                                            <!-- <a href="#" class="red"><i class="ace-icon fa fa-trash-o bigger-150" title="Eliminar"></i></a><span class="vbar"></span> --></center></td>
                                            <?php include('../modal/editarEmpresa.php'); ?>    
                                    </tr><?php
                                }?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
<script src="../assets/js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript">
            if('ontouchstart' in document.documentElement) document.write("<script src='../assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
        </script>
        <script src="../assets/js/bootstrap.min.js"></script>

        <!-- page specific plugin scripts -->
        <script src="../assets/js/jquery.dataTables.min.js"></script>
        <script src="../assets/js/jquery.dataTables.bootstrap.min.js"></script>
        <script src="../assets/js/dataTables.buttons.min.js"></script>
        <script src="../assets/js/buttons.flash.min.js"></script>
        <script src="../assets/js/buttons.html5.min.js"></script>
        <script src="../assets/js/buttons.print.min.js"></script>
        <script src="../assets/js/buttons.colVis.min.js"></script>
        <script src="../assets/js/dataTables.select.min.js"></script>

        <!-- ace scripts -->
        <script src="../assets/js/ace-elements.min.js"></script>
        <script src="../assets/js/ace.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#t_client').DataTable({
            "info": false,
            "language": {
            "url": "../assets/js/Spanish.json"
            }
        });
    });
</script>
</body>
</html>
<?php
require 'footer.html';
?>
