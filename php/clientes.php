<?php
session_start();
require_once('conexion.php');
date_default_timezone_set('America/Lima');
$clientes_menu=1;
$nomb=$_SESSION['nom'];
/*$valida=mysqli_num_rows(mysqli_query($conex,"SELECT Num_doc FROM usuario where Num_doc='$nomb'"));
if ($nomb== null or $nomb=="" or $valida==0) {
  header("location:../index.html");
}*/
$permiso="Clientes";
$rs_user=mysqli_fetch_row(mysqli_query($conex,"SELECT Id_user FROM usuario where Num_doc='$nomb'"));
$id_user=$rs_user[0];
$sql = mysqli_query($conex, "SELECT p.*, d.* FROM permiso p INNER JOIN usuario_permiso d ON p.idpermiso = d.idpermiso WHERE d.id_user = $id_user AND p.nombre = '$permiso'");
$existe = mysqli_fetch_all($sql);
if (empty($existe) && $id_user != 1) {
    header("Location: permisos.php");
}
require 'header.php';
include_once '../modal/nuevo_cliente.php';
$nomb=$_SESSION['nom'];
$valida=mysqli_num_rows(mysqli_query($conex,"SELECT Num_doc FROM usuario where Num_doc='$nomb'"));
if ($nomb== null or $nomb=="" or $valida==0) {
  header("location:index.html");
}
$rs_cliente=mysqli_query($conex,"SELECT*FROM clientes");
?>
<!DOCTYPE html>
<html>
<head>
    <title></title>
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="../assets/font-awesome/4.5.0/css/font-awesome.min.css" />
    <!-- text fonts -->
    <link rel="stylesheet" href="../assets/css/fonts.googleapis.com.css" />
    <!-- ace styles -->
    <link rel="stylesheet" href="../assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />
    <link rel="stylesheet" href="../assets/css/ace-skins.min.css" />
    <link rel="stylesheet" href="../assets/css/ace-rtl.min.css" />
    <!-- ace settings handler -->
    <script src="../assets/js/ace-extra.min.js"></script>
    <style type="text/css">
        .disabled {
            cursor: not-allowed;
            pointer-events: none;
        }
    </style>
</head>
<body>
    <div class="modal fade" id="modalPagos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="panel-title"><span class="glyphicon glyphicon-list" aria-hidden="true"></span> Detalles de Pago</h3>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3">
                            <label>Fecha: </label>
                            <input type="date" class="form-control" id="fechaModal" value="<?= date('Y-m-d') ?>">
                        </div>
                        <div class="col-md-3">
                            <label>Hora: </label>
                            <input type="time" class="form-control" id="horaModal" value="<?= date('G:i:s') ?>">
                        </div>
                        <div class="col-md-2">
                            <label>Forma de Pago:</label>
                            <select id="cboFormaPagoModal" class="form-control">
                                <option value="" selected disabled>[ SELECCIONE ]</option>
                                <option value="CONT">CONTADO</option>
                                <option value="CRED">CREDITO</option>
                                <option value="YAPE">YAPE</option>
                                <option value="PLIN">PLIN</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label>Monto:</label>
                            <input type="text" class="form-control" id="montoModal">
                        </div>
                        <div class="col-md-1">
                            <label>&nbsp;&nbsp;</label>
                            <button type="button" class="btn btn-primary btn-xs btn-block" id="guardarDetalleModal"><i class="fa fa-plus"></i></button>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 15px;">
                        <div class="col-md-12">
                            <table class="table table-striped table-sm">
                                <thead>
                                    <tr>
                                        <th>Fecha</th>
                                        <th>Hora</th>
                                        <th>Forma Pago</th>
                                        <th>Monto</th>
                                    </tr>
                                </thead>
                                <tbody id="tbodyDetalleDeuda">
                                    <tr>
                                       <td colspan="4">Cargando...</td> 
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="panel-footer text-right">
                    <button type="button" class="btn btn-secondary btn-xs" data-dismiss="modal">Cerrar</button>
                    <!-- <button type="button" class="btn btn-primary btn-xs">Guardar</button> -->
                </div>
            </div>
        </div>
    </div>
    <div class="main-content">
    <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">Home</a>
                </li>
                <li class="active">Clientes</li>
            </ul><!-- /.breadcrumb -->
        </div>

        <div class="page-content">
            <div class="page-header">
                <button type="button" class="btn btn-white btn-sm btn-success btn-round" data-toggle="modal" data-target="#modal-client">Agregar Cliente <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span></button>

            </div><!-- /.page-header -->
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><span class="glyphicon glyphicon-list" aria-hidden="true"></span> Registro de Clientes</h3>
                        </div>
                        <div class="table-responsive">
                            <table id="t_client" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr class="info">
                                        <th>IT</th>
                                        <th>Nombre</th>
                                        <th>Documento</th>
                                        <th>Telefono</th>
                                        <th>Email</th>
                                        <th>Lim. Crédito</th>
                                        <th>Cant. días</th>
                                        <th>Estado</th>
                                        <th>OP</th>
                                        <th>Detalle</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                while ($clientes=mysqli_fetch_row($rs_cliente)) { ?>
                                    <tr>
                                        <td><?php echo $clientes[0]; ?></td>
                                        <td><?php echo $clientes[2]; ?></td>
                                        <!-- <td><?php echo $clientes[3]; ?></td> -->
                                        <td><?php echo $clientes[4]; ?></td>
                                        <td><?php echo $clientes[6]; ?></td>
                                        <td><?php echo $clientes[7]; ?></td>
                                        <td><?php echo $clientes[8]; ?></td>
                                        <td><?php echo $clientes[9]; ?></td>
                                        <td class="center"><?php
                                              if ($clientes[10]==1 or $clientes[10]==3) {
                                                echo '<span style="font-size: 10px;" class="label label-success arrowed-in arrowed-in-right">Activo</span>';
                                              }else{
                                                 echo '<span style="font-size: 10px" class="label label-danger arrowed">Inactivo</span>';
                                              }?>
                                        </td>
                                        <td class="center">
                                            <a href="#" class="blue"><i class="ace-icon fa fa-pencil bigger-150" data-toggle="modal" data-target="#edit_<?php echo $clientes[0]; ?>" title="Editar cliente"></i></a><span class="vbar"></span>
                                            <?php if ($clientes[10]==0) { $estado="disabled";}else{$estado="";} ?>
                                            <a href="#" class="red <?php echo $estado ?>"><i class="ace-icon fa fa-trash-o bigger-150" data-toggle="modal" data-target="#delete_<?php echo $clientes[0]; ?>" title="Editar cliente"></i></a><span class="vbar"></span>
                                        </td>
                                            <?php 
                                                include '../modal/editarCliente.php'; 
                                                $rs_deudas=mysqli_query($conex,"SELECT * FROM deudas WHERE Id_client=$clientes[0]");
                                            ?>

                                        <td class="center">
                                            <div class="action-buttons">
                                                <a href="#" class="green bigger-140 show-details-btn" title="Show Details">
                                                    <i class="ace-icon fa fa-angle-double-down"></i>
                                                    <span class="sr-only">Details</span>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="detail-row">
                                        <td colspan="8">
                                            <div class="table-detail">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-9">
                                                        <div class="panel panel-success">
                                                            <div class="panel-heading">
                                                                <h3 class="panel-title"><span class="glyphicon glyphicon-list" aria-hidden="true"></span> Reporte de deudas</h3>
                                                            </div>
                                                        
                                                            <table id="t_deuda" class="table table-striped table-bordered table-hover">
                                                                <thead>
                                                                    <tr class="info">
                                                                        <th>ID</th>
                                                                        <th>Cliente</th>
                                                                        <th>FECHA</th>
                                                                        <th>HORA</th>
                                                                        <th>TOTAL</th>
                                                                        <th>ESTADO</th>
                                                                        <th>ACCIÓN</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody><?php $x=1;
                                                                while ($deudas=mysqli_fetch_row($rs_deudas)) { ?>
                                                                    <tr>
                                                                        <td><?php echo $x?></td>
                                                                        <td><?php echo $clientes[2]?></td>
                                                                        <td><?php echo $deudas[3]?></td>
                                                                        <td><?php echo $deudas[4]?></td>
                                                                        <td><?php echo $deudas[6]?></td>
                                                                        <!-- <td><?php echo $deudas[7]?></td> -->
                                                                        <td class="td-deuda-<?= $deudas[0]; ?> td-deuda">
                                                                            <?php if($deudas[7] == 1): ?>
                                                                                <i class="ace-icon fa fa-times text-danger btn-pagar" data-estado="<?= $deudas[7]; ?>" data-id="<?= $deudas[0] ?>" style="cursor:pointer;"></i>&nbsp;&nbsp;Pediente
                                                                            <?php else: ?>
                                                                                <i class="ace-icon fa fa-check text-success btn-pagar" data-estado="<?= $deudas[7]; ?>" data-id="<?= $deudas[0] ?>" style="cursor:pointer;"></i>&nbsp;&nbsp;Pagado
                                                                            <?php endif; ?>
                                                                        </td>
                                                                        <td>
                                                                            <button type="button" class="btn btn-xs btn-<?= (($deudas[7] == 1)?'info': 'success' ) ?> btn-pagar-modal btn-pagar-modal-<?= $deudas[0] ?>" data-monto="<?= $deudas[6]; ?>" data-estado="<?= $deudas[7]; ?>" data-id="<?= $deudas[0] ?>" data-id-usuario="<?= $clientes[0] ?>"> <?= (($deudas[7] == 1)?'Pagar': 'Detalles' ) ?></button>
                                                                        </td>
                                                                    </tr><?php $x++;
                                                                }?>
                                                                </tbody>
                                                            </table>
                                                        </div>        
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr><?php
                                    }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
        <script src="../assets/js/jquery-2.1.4.min.js"></script>
        <!-- <script type="text/javascript">
            if('ontouchstart' in document.documentElement) document.write("<script src='../assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
        </script> -->
        <script src="../assets/js/bootstrap.min.js"></script>

        <!-- page specific plugin scripts -->
        <script src="../assets/js/jquery.dataTables.min.js"></script>
        <script src="../assets/js/jquery.dataTables.bootstrap.min.js"></script>
        <script src="../assets/js/dataTables.buttons.min.js"></script>
        <script src="../assets/js/buttons.flash.min.js"></script>
        <script src="../assets/js/buttons.html5.min.js"></script>
        <script src="../assets/js/buttons.print.min.js"></script>
        <script src="../assets/js/buttons.colVis.min.js"></script>
        <script src="../assets/js/dataTables.select.min.js"></script>

        <!-- ace scripts -->
        <script src="../assets/js/ace-elements.min.js"></script>
        <script src="../assets/js/ace.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                let fechaActual = "<?= date('Y-m-d'); ?>";
                let horaActual = "<?= date('G:i:s'); ?>";
                let estadoDeuda = 0;
                let IdDeuda = 0;
                let IdUsuario = 0;
                const arrayFormaPago = {'CONT':'CONTADO','CRED' : 'CREDITO','YAPE':'YAPE','PLIN': 'PLIN'};
                let totalDeuda = 0;
                let pagado = 0;

                $(".btn-pagar-modal").click(function(){
                    estadoDeuda = 0;
                    IdDeuda = 0;
                    IdUsuario = 0;
                    totalDeuda = 0;
                    pagado = 0;

                    IdUsuario = $(this).attr('data-id-usuario');
                    
                    estadoDeuda = $(this).attr('data-estado');
                    IdDeuda = $(this).attr('data-id');
                    totalDeuda = parseFloat($(this).attr('data-monto'));
                    console.log(totalDeuda);
                    listarDetalle(IdDeuda,totalDeuda);
                    $("#modalPagos").modal('show');
                    $("#fechaModal").val(fechaActual);
                    $("#horaModal").val(horaActual);
                });

                $("#guardarDetalleModal").click(function(){
                    if($("#fechaModal").val()!="" && $("#horaModal").val()!="" && $("#cboFormaPagoModal").val() != null && $("#montoModal").val()!=""){
                        deuda = totalDeuda - parseFloat($("#montoModal").val());
                        datos = {
                            'fecha'     : $("#fechaModal").val(),
                            'hora'      : $("#horaModal").val(),
                            'forma_pago': $("#cboFormaPagoModal").val(),
                            'monto'     : $("#montoModal").val(),
                            'estado'    : estadoDeuda,
                            'id'        : IdDeuda,
                            'id_user'   : IdUsuario,
                            'deuda'     : deuda
                        }
                        $.ajax({
                          url: 'pagar_deuda.php',
                          type: 'GET',
                          data:datos,
                          success: function (data) {
                            // console.log(data);
                            // console.log("pagado");
                            // $("#modalPagos").modal('hide');
                            e = JSON.parse(data); 
                            html = '';

                            if(e.estado == 2){
                                html = `<i class="ace-icon fa fa-check text-success btn-pagar" data-estado="${e.estado}" data-id="${IdDeuda}" style="cursor:pointer;"></i>&nbsp;&nbsp;Pagado`;
                            }else{
                                html = `<i class="ace-icon fa fa-times text-danger btn-pagar" data-estado="${e.estado}" data-id="${IdDeuda}" style="cursor:pointer;"></i>&nbsp;&nbsp;Pediente`;
                            }
                            $(".td-deuda-"+IdDeuda).html(html);
                            listarDetalle(IdDeuda,deuda,1);

                            if((deuda) <= 0){
                                $(".btn-pagar-modal-"+IdDeuda).text('Detalles');
                                $(".btn-pagar-modal-"+IdDeuda).addClass('btn-success');
                                $(".btn-pagar-modal-"+IdDeuda).removeClass('btn-info');
                            }
                          },
                        });
                        console.log(datos);
                    }else{
                        alert("Todos los campos son obligatorios");
                    }
                });

                $('.show-details-btn').on('click', function(e) {
                    e.preventDefault();
                    $(this).closest('tr').next().toggleClass('open');
                    $(this).find(ace.vars['.icon']).toggleClass('fa-angle-double-down').toggleClass('fa-angle-double-up');
                });

                function listarDetalle(id,monto,est = 1){
                    $.ajax({
                        url: 'listar_detalle_deuda.php',
                        type: 'GET',
                        data: {id:id},
                        success: function (data) {
                            datos = JSON.parse(data);
                            html = '';
                            if(!$.isEmptyObject(datos)){
                                datos.forEach((e,i)=>{
                                    html+=`<tr>
                                        <td>${e.Fecha}</td>
                                        <td>${e.Hora}</td>
                                        <td>${arrayFormaPago[e.Forma_pago]}</td>
                                        <td>${e.Monto}</td>
                                    </tr>`;

                                    pagado += parseFloat(e.Monto);
                                });

                                $("#tbodyDetalleDeuda").html(html);
                            }else{
                                $("#tbodyDetalleDeuda").html(`<tr><td colspan="4" class="text-center">No hay datos</td></tr>`);
                            }   

                            if(est = 1){
                                totalDeuda = totalDeuda - pagado;
                                $("#montoModal").val(totalDeuda);
                            }
                            
                            if(parseFloat(totalDeuda) <= 0){
                                $("#fechaModal").attr('disabled',true);
                                $("#horaModal").attr('disabled',true);
                                $("#cboFormaPagoModal").attr('disabled',true);
                                $("#montoModal").attr('disabled',true);
                                $("#guardarDetalleModal").attr('disabled',true);
                                $("#montoModal").val(0);
                            }else{
                                $("#fechaModal").attr('disabled',false);
                                $("#horaModal").attr('disabled',false);
                                $("#cboFormaPagoModal").attr('disabled',false);
                                $("#montoModal").attr('disabled',false);
                                $("#guardarDetalleModal").attr('disabled',false);
                            }
                        }
                    })
                }
            });

        </script>
</body>
</html>
<?php
require 'footer.html';
?>
