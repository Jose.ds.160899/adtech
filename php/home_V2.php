<?php
session_start();
#error_reporting(0);
require_once('conexion.php');
$home_menu=1;
require 'header.php';
$nomb=$_SESSION['nom'];
$valida=mysqli_num_rows(mysqli_query($conex,"SELECT Num_doc FROM usuario where Num_doc='$nomb'"));
if ($nomb== null or $nomb=="" or $valida==0) {
  header("location:index.html");
}
$usuario=mysqli_fetch_row(mysqli_query($conex,"SELECT*FROM usuario WHERE Num_doc='$nomb'"));
/*$rs_inventario=mysqli_query($conex,"SELECT*FROM articulo,stock,tipo_articulo WHERE articulo.Id_art = stock.Id_art AND articulo.Id_tipo_art=tipo_articulo.Id_tipo_art AND stock.stock>0");*/
$rs_Ticket=mysqli_fetch_row(mysqli_query($conex,"SELECT CONCAT('T001-',LPAD(CONVERT(RIGHT(Num_comp,6)+1, CHAR ), 6,'0')) AS Serie FROM Correlativo WHERE Tipo_comp='Ticket'"));
$rs_correlativo=mysqli_query($conex,"SELECT * FROM Correlativo");
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <title></title>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
        <link rel="stylesheet" href="assets/font-awesome/4.5.0/css/font-awesome.min.css" />
        <!-- text fonts -->
        <link rel="stylesheet" href="assets/css/fonts.googleapis.com.css" />
        <!-- ace styles -->
        <link rel="stylesheet" href="assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />
        <link rel="stylesheet" href="assets/css/ace-skins.min.css" />
        <link rel="stylesheet" href="assets/css/ace-rtl.min.css" />
        <!-- ace settings handler -->
        <script src="assets/js/ace-extra.min.js"></script>
       <style type="text/css">
         .marito{
          padding: 2px;
          margin: 0px;
          font-size: 11px;
          border-radius: 4px;
          font-family: 'Verdana';
         }
          input[type="number"]::-webkit-outer-spin-button,
          input[type="number"]::-webkit-inner-spin-button {
              -webkit-appearance: none;
              margin: 0;
          }
          input[type="number"] {
              -moz-appearance: textfield;
          }
       </style>
</head>
<body>
  <div class="main-content">
    <div class="main-content-inner">
      <div class="breadcrumbs ace-save-state" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-shopping-cart home-icon"></i>
                    <a href="#"><?php echo $usuario[7] ?></a>
                </li>
                <li class="active"><?php echo $usuario[1];?></li>
            </ul><!-- /.breadcrumb -->
      </div>
      <div class="page-content">
        <div class="col-md-12">
              <div class="col-md-3">
                <div class="form-group" style="margin-bottom: 4px">
                    <label class="control-label text-primary btn-round" >Correlativo: <b><?php echo $rs_Ticket[0];?></b></label>
                </div>
              </div>
              <!-- <div class="col-md-6">

                  <label class="control-label col-sm-2 text-primary btn-round" for="">Cliente:</label>
                  <div class="col-sm-3">
                    <input class="marito" type="text" maxlength="50" class="form-control" id="item-name" name="item-name" placeholder="Ingresa nombre generico" required="" autofocus="">
                  </div>

              </div> -->
        </div>
        <div class="row" id="order">
          <?php include 'order.php';?>
        </div><!-- /.row -->
      </div><!-- /.page-content -->
    </div>
  </div><!-- /.main-content -->
  <!-- page specific plugin scripts -->
  <script>

    function agregar(id)
    {
      var cant=document.getElementById('cant'+id).value;
      var id_art=document.getElementById('id_art'+id).value;
      var id_stock=document.getElementById('id_stock'+id).value;
      var id_user=document.getElementById('id_user'+id).value;
      var precio_art=document.getElementById('precio_art'+id).value;
      var stock=document.getElementById('stock'+id).value;
      //validacion
      var valida = stock - cant;
        if(valida < 0){
          alert('No hay stock para vender esa cantidad!');
        }else{
          var parametros = {
            "cant" : cant,
            "id_art" : id_art,
            "id_stock" : id_stock,
            "id_user" : id_user,
            "precio_art" : precio_art
          };
          $.ajax({
            data:  parametros, //datos que se envian a traves de ajax
            url:   'guardar_carrito.php', //archivo que recibe la peticion
            type:  'post', //método de envio
            /*beforeSend: function () {
                    $("#Mycart").html("Procesando...");
            },*/
            /*success:  function (response) { //una vez que el archivo recibe el request lo procesa y lo devuelve
              $("#Mycart").html(response);
            }*/
            success: function (data) {
              console.log(data);
              showOrder();
            }
          });
        }
      }


  function showOrder(){
  $.ajax({
      url: 'order.php',
      type: 'post',
      success: function (data) {
        $('#order').html(data);
      },
      error: function(){
        eMsg('297');
      }
    });
}//end showOrder

  </script>

</body>
</html>
<?php
require 'footer.html';
?>
