<?php
session_start();
require_once('conexion.php');
date_default_timezone_set('America/Lima');
$rpteinv_menu=1;
$nomb=$_SESSION['nom'];
$permiso="Rpte Inventario";
$rs_user=mysqli_fetch_row(mysqli_query($conex,"SELECT Id_user FROM usuario where Num_doc='$nomb'"));
$id_user=$rs_user[0];
$sql = mysqli_query($conex, "SELECT p.*, d.* FROM permiso p INNER JOIN usuario_permiso d ON p.idpermiso = d.idpermiso WHERE d.id_user = $id_user AND p.nombre = '$permiso'");
$existe = mysqli_fetch_all($sql);
if (empty($existe) && $id_user != 1) {
    header("Location: permisos.php");
}
require 'header.php';
$rs_inventario=mysqli_query($conex,"SELECT * FROM articulo,stock,tipo_articulo WHERE articulo.Id_art = stock.Id_art AND articulo.Id_tipo_art=tipo_articulo.Id_tipo_art AND Stock!=0 AND articulo.Id_art!=44");
?>
<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta charset="UTF-8"/>
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="../assets/font-awesome/4.5.0/css/font-awesome.min.css" />
    <!-- text fonts -->
    <link rel="stylesheet" href="../assets/css/fonts.googleapis.com.css" />
    <!-- ace styles -->
    <link rel="stylesheet" href="../assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />
    <link rel="stylesheet" href="../assets/css/ace-skins.min.css" />
    <link rel="stylesheet" href="../assets/css/ace-rtl.min.css" />
    <link rel="stylesheet" href="../assets/css/jquery-ui.custom.min.css" />
    <link rel="stylesheet" href="../assets/css/chosen.min.css" />
    <!-- ace settings handler -->
    <script src="../assets/js/ace-extra.min.js"></script>
</head>
<body>
<div class="main-content">
    <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">Home</a>
                </li>
                <li class="active">Inventario</li>
            </ul><!-- /.breadcrumb -->
        </div>

        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><span class="glyphicon glyphicon-list" aria-hidden="true"></span> Inventario de Productos</h3>
                        </div>
                            <div class="table-responsive">
                                <table id="Myorder" class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr class="info">
                                            <th>Item</th>
                                            <th>Nombre</th>
                                            <th>Proveedor</th>
                                            <th>Fecha Compra</th>
                                            <th>Costo</th>
                                            <th>Precio</th>
                                            <!-- <th>Margen S/</th>
                                            <th>Margen %</th> -->
                                            <th>Stock</th>
                                        </tr>
                                    </thead>
                                    <tbody><?php $i=1; $total=0;$gana=0;$stock=0;
                                    		$dateNow = date('Y-m');
                                        while ($inventario=mysqli_fetch_row($rs_inventario)) {
                                            $fab=mysqli_fetch_row(mysqli_query($conex,"SELECT * FROM proveedor WHERE Id_prov=$inventario[7]"));
                                            $total=$total+$inventario[5];
                                            $gana=$gana+$inventario[8];
                                            $stock=$stock+$inventario[12];
                                        	$xDate = date_create($inventario[14]);
                                        	$xDate = date_format($xDate,'Y-m');
						                    $class = 'text-success';
						                    if($xDate <= $dateNow){
						                        $class = 'danger';
						                    } ?>
                                        <tr align="center" class="<?php echo $class; ?>">
                                            <td><?php echo $i?></td>
                                            <td align="left"><?php echo $inventario[4];?></td>
                                            <td align="left"><?php echo $fab[2];?></td>
                                            <td><?php echo $inventario[13];?></td>
                                            <td><?php echo $inventario[8];?></td>
                                            <td><?php echo $inventario[5];?></td>
                                            <!-- <td><?php echo number_format($inventario[5]-$inventario[7],2);?></td>
                                            <td><?php echo number_format(((($inventario[5]/$inventario[7])*100)-100),2).'%';?></td> -->
                                            <td><?php echo $inventario[12];?></td>
                                            <!-- <td><?php echo $inventario[13];?>
                                            	<?php if($xDate <= $dateNow): ?>
					                                <span class="label label-danger">!</span>
					                            <?php endif; ?>
                                            </td> -->
                                        </tr><?php $i++; } ?>
                                    </tbody>
                                    <tr class="info">
                                        <td align="right"><b>Total: </b></td>
                                        <td colspan="3"><?php echo ($i-1).' Productos'?></td>
                                        <td align="center"><b><?php echo number_format($gana,2); ?></b></td>
                                        <td align="center"><b><?php echo number_format($total,2); ?></b></td>
                                        <td align="center"><b><?php echo number_format($stock); ?></b></td>
                                    </tr>
                                </table>
                            </div>
                    </div>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
<script src="../assets/js/jquery-2.1.4.min.js"></script>
<script src="../assets/js/chosen.jquery.min.js"></script>
<script type="text/javascript">
    if('ontouchstart' in document.documentElement) document.write("<script src='../assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
</script>
<script src="../assets/js/bootstrap.min.js"></script>

<!-- page specific plugin scripts -->
<script src="../assets/js/jquery.dataTables.min.js"></script>
<script src="../assets/js/jquery.dataTables.bootstrap.min.js"></script>
<script src="../assets/js/dataTables.buttons.min.js"></script>
<script src="../assets/js/buttons.flash.min.js"></script>
<script src="../assets/js/buttons.html5.min.js"></script>
<script src="../assets/js/buttons.print.min.js"></script>
<script src="../assets/js/jszip.min.js"></script>
<script src="../assets/js/buttons.colVis.min.js"></script>
<script src="../assets/js/dataTables.select.min.js"></script>
<script src="../assets/js/chosen.jquery.min.js"></script>
<!-- ace scripts -->
<script src="../assets/js/ace-elements.min.js"></script>
<script src="../assets/js/ace.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
         //$('#item-id').select2();
        $('#Myorder').DataTable({
            "iDisplayLength": 20,
            "info": false,
            "language": {
            "url": "../assets/js/Spanish.json"
            },
            dom: 'Bfrtip',
            buttons: [
                {"extend": 'print', "text": '<span class = "glyphicon glyphicon-print"> </span>', "className": 'btn btn-success btn-xs'},
                {"extend": 'excel', "text": '<span class = "glyphicon glyphicon-list"></span>', "className": 'btn btn-success btn-xs'}
            ]
        });
    });
</script>
</body>
</html>
<?php
require 'footer.html';
?>
