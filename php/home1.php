<?php
session_start();
#error_reporting(0);
require_once('conexion.php');
$home_menu=1;
require 'header.php';
$nomb=$_SESSION['nom'];
$valida=mysqli_num_rows(mysqli_query($conex,"SELECT Num_doc FROM usuario where Num_doc='$nomb'"));
if ($nomb== null or $nomb=="" or $valida==0) {
  header("location:index.html");
}
$usuario=mysqli_fetch_row(mysqli_query($conex,"SELECT*FROM usuario WHERE Num_doc='$nomb'"));
/*$rs_inventario=mysqli_query($conex,"SELECT*FROM articulo,stock,tipo_articulo WHERE articulo.Id_art = stock.Id_art AND articulo.Id_tipo_art=tipo_articulo.Id_tipo_art AND stock.stock>0");*/
$rs_inventario=mysqli_query($conex,"SELECT a.Nombre,a.Composicion,a.Gramos,c.Nom_tipo_art,a.Fabricante,a.Precio,b.Stock,b.Fecha_Venc,b.id_stock,a.id_art FROM articulo a INNER JOIN Stock b ON a.Id_art = b.Id_art INNER JOIN tipo_articulo c ON a.Id_tipo_art=c.Id_tipo_art AND b.stock>0");
$rs_cant=mysqli_num_rows($rs_inventario);
$rs_carrito=mysqli_query($conex,"SELECT*FROM carrito,articulo,stock WHERE carrito.Id_art=articulo.Id_art AND carrito.Id_stock=stock.Id_stock");
$rs_Ticket=mysqli_fetch_row(mysqli_query($conex,"SELECT CONCAT('T001-',LPAD(CONVERT(RIGHT(Num_comp,6)+1, CHAR ), 6,'0')) AS Serie FROM Correlativo WHERE Tipo_comp='Ticket'"));
$rs_correlativo=mysqli_query($conex,"SELECT * FROM Correlativo");
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <title></title>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
        <link rel="stylesheet" href="assets/font-awesome/4.5.0/css/font-awesome.min.css" />
        <!-- text fonts -->
        <link rel="stylesheet" href="assets/css/fonts.googleapis.com.css" />
        <!-- ace styles -->
        <link rel="stylesheet" href="assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />
        <link rel="stylesheet" href="assets/css/ace-skins.min.css" />
        <link rel="stylesheet" href="assets/css/ace-rtl.min.css" />
        <!-- ace settings handler -->
        <script src="assets/js/ace-extra.min.js"></script>
       <style type="text/css">
         .marito{
          padding: 2px;
          margin: 0px;
          font-size: 11px;
          border-radius: 4px;
          font-family: 'Verdana';
         }
          input[type="number"]::-webkit-outer-spin-button,
          input[type="number"]::-webkit-inner-spin-button {
              -webkit-appearance: none;
              margin: 0;
          }
          input[type="number"] {
              -moz-appearance: textfield;
          }
       </style>
</head>
<body>
  <div class="main-content">
    <div class="main-content-inner">
      <div class="breadcrumbs ace-save-state" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-shopping-cart home-icon"></i>
                    <a href="#"><?php echo $usuario[7] ?></a>
                </li>
                <li class="active"><?php echo $usuario[1];?></li>
            </ul><!-- /.breadcrumb -->
      </div>
      <div class="page-content">
        <div class="col-md-12">
              <div class="col-md-3">
                <div class="form-group" style="margin-bottom: 4px">
                    <label class="control-label text-primary btn-round" >Correlativo: <b><?php echo $rs_Ticket[0];?></b></label>
                </div>
              </div>
              <!-- <div class="col-md-6">

                  <label class="control-label col-sm-2 text-primary btn-round" for="">Cliente:</label>
                  <div class="col-sm-3">
                    <input class="marito" type="text" maxlength="50" class="form-control" id="item-name" name="item-name" placeholder="Ingresa nombre generico" required="" autofocus="">
                  </div>

              </div> -->
            </div>
          <div class="row">
            <div class="col-md-7">
              <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title"><span class="glyphicon glyphicon-list" aria-hidden="true"></span> Consulta Productos</h3>
                </div>
                <div class="table-responsive">
                  <table id="Myorder" class="table table-striped table-bordered table-hover">
                    <thead>
                      <tr class="info">
                        <th>Producto</th>
                        <th>Composición</th>
                        <th>Fabricante</th>
                        <th>Precio</th>
                        <th>Stock</th>
                        <th>Vence</th>
                        <th style="width: 8%">Cant</th>
                        <th>Cargar</th>
                      </tr>
                    </thead>
                    <tbody><?php $i=1;
                      while ($venta=mysqli_fetch_row($rs_inventario)) {
                        $date = date_create($venta[7]);
                        ?>
                      <tr class="p-0">
                        <td><?php echo $venta[0] ?></td>
                        <td style="font-size: 8px"><?php echo $venta[1].' '.$venta[2].' / '.$venta[3]; ?></td>
                        <td><?php echo $venta[4] ?></td>
                        <td><?php echo $venta[5] ?></td>
                        <td><?php echo $venta[6] ?></td>
                        <td><?php echo date_format($date,'m-Y') ?></td>
                        <form method="POST" action="guardar_carrito.php">
                          <td><input style="width: 90%;padding: 0px;font-size: 12px;" type="number" min="0" id="cant" name="cant"></td>
                          <td align="center"><button type="submit" class="btn btn-white btn-success btn-xs btn-round"><span class="glyphicon glyphicon-shopping-cart" title="Agregar al Carrito" aria-hidden="true"></span></button></td>
                          <input type="hidden" name="id" value="<?php echo $venta[9];?>">
                          <input type="hidden" name="id_stock" value="<?php echo $venta[8];?>">
                          <input type="hidden" name="id_user" value="<?php echo $usuario[0];?>">
                          <input type="hidden" name="precio_art" value="<?php echo $venta[5];?>">
                          <input type="hidden" name="ticket" value="<?php echo $rs_Ticket[0];?>">
                          </form>
                      </tr>
                      <?php $i++; } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div><!-- /.col -->
            <div class="col-md-5">
              <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> Carrito</h3>
                </div>
                <div class="table-responsive">
                  <form method="POST" action="guardar_venta.php">
                    <table id="Mycart" class="table table-striped table-bordered table-hover">
                      <thead>
                        <tr class="info">
                          <th>Producto</th>
                          <th>Precio</th>
                          <th>Cant</th>
                          <th>Sub</th>
                          <th>Elimina</th>
                        </tr>
                      </thead>
                      <tbody><?php
                        $total = 0;
                        $rows=mysqli_num_rows($rs_carrito);
                        while ($carrito=mysqli_fetch_row($rs_carrito)) {
                            $precio=number_format($carrito[10],2);
                            $cant=$carrito[2];
                            $subtotal=$precio*$cant;
                            $total += $subtotal;
                          ?>
                        <tr align="center">
                          <td align="left"><?php echo $carrito[9] ?></td>
                          <td><?php echo 'S/ '.number_format($carrito[10],2) ?></td>
                          <td><?php echo $carrito[2] ?></td>
                          <td><?php echo number_format($subtotal,2); ?></td>
                          <td align="center"><button type="button" class="btn btn-white btn-danger btn-xs btn-round" ><a class="text-danger" href="guardar_carrito.php?op=1&idcart=<?php echo $carrito[0];?>"><span class="glyphicon glyphicon-trash" title="Eliminar Item" aria-hidden="true"></span></a></button></td>
                        </tr><?php } ?>
                      </tbody>
                      <tr>
                        <td></td>
                        <td></td>
                        <td align="right"><strong>Total:</strong></td>
                        <td align="center">
                            <strong><?php echo number_format($total, 2); ?></strong>
                        </td>
                        <td align="center">
                          <?php if($total > 0){ ?>
                            <div>
                              <button type="submit" class="btn btn-white btn-success btn-xs btn-round"><a class="text-success" href="guardar_venta.php?row=<?php echo $rows;?>&corre=<?php echo $rs_Ticket[0];?>&tot_vta=<?php echo number_format($total,2);?>">Venta<span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span></a></button>
                          </div><?php } ?>

                        </td>
                      </tr>
                    </table>
                  </form>
                </div>
              </div>
            </div><!-- /.col -->
          </div><!-- /.row -->
      </div><!-- /.page-content -->
    </div>
  </div><!-- /.main-content -->
  <script src="assets/js/jquery-2.1.4.min.js"></script>
  <script type="text/javascript">
      if('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
  </script>
  <script src="assets/js/bootstrap.min.js"></script>

  <!-- page specific plugin scripts -->
  <script src="assets/js/jquery.dataTables.min.js"></script>
  <script src="assets/js/jquery.dataTables.bootstrap.min.js"></script>
  <script src="assets/js/dataTables.buttons.min.js"></script>
  <script src="assets/js/buttons.flash.min.js"></script>
  <script src="assets/js/buttons.html5.min.js"></script>
  <script src="assets/js/buttons.print.min.js"></script>
  <script src="assets/js/buttons.colVis.min.js"></script>
  <script src="assets/js/dataTables.select.min.js"></script>

  <!-- ace scripts -->
  <script src="assets/js/ace-elements.min.js"></script>
  <script src="assets/js/ace.min.js"></script>
  <script type="text/javascript">
      $(document).ready(function() {
          $('#Myorder').DataTable({
              "iDisplayLength": 20,
              "info": false,
              "language": {
              "url": "assets/js/Spanish.json"
              }
          });
      });
  </script>
  <script type="text/javascript">
    $(document).ready(function() {
        $('#Mycar').DataTable({
            "info": false,
            "language": {
            "url": "assets/js/Spanish.json"
            },
            "searching": false,

        });
    });
    var mostrarValor = function(x){
      document.getElementById('correl').value=x;
    }
  </script>
</body>
</html>
<?php
require 'footer.html';
?>
