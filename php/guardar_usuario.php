<?php
require_once 'conexion.php';
session_start();
$nomb=$_SESSION['nom'];
if ($nomb== null || $nomb=="") { ?>
  <div class="container"><br>
    <div class="alert alert-danger" role="alert"><strong>Atención! </strong>No tiene permiso</div>
  </div><?php
  header("location:../index.html");
}
if(isset($_GET['id'])){
  $id_user=$_GET['id'];
  mysqli_query($conex,"UPDATE usuario SET Estado=0 WHERE Id_user=$id_user");
  echo '<script language="javascript">alert("Usuario eliminado correctamente");window.location.href="usuarios.php";</script>';
}else{
  $nombre=$_POST['user-name'];
  $tipo=$_POST['tipo'];
  $num_doc=$_POST['doc'];
  $dir=$_POST['dir'];
  $tel=$_POST['cel'];
  $mail=$_POST['mail'];
  $cargo=$_POST['cargo'];
  $pass=$_POST['pwd'];
  $actualiza=$_POST['actualiza'];
  $user_id=$_POST['user_id'];
  $estado=1;
  try {
    if ($actualiza==1) {
      mysqli_query($conex,"UPDATE usuario SET Nombre='$nombre',Tipo_doc='$tipo',Num_doc='$num_doc',Direccion='$dir',Fono='$tel',Email='$mail',Clave='$pass',Estado=$estado WHERE Id_user=$user_id");
      header('location:usuarios.php');
    }else{
      $existe=mysqli_num_rows(mysqli_query($conex,"SELECT * FROM usuario WHERE Num_doc='$num_doc'"));
      if ($existe>=1) {
        echo '<script>alert("El DNI ya se encuentra registrado");window.location="usuarios.php";</script>';
      }else{
        mysqli_query($conex,"INSERT INTO usuario VALUES(0,'$nombre','$tipo','$num_doc','$dir',$tel,'$mail','$cargo','$pass','1')");
        $usuario=mysqli_fetch_row(mysqli_query($conex,"SELECT Id_user FROM `usuario` ORDER BY Id_user DESC LIMIT 1;"));
        $iduser=$usuario[0];
        $permisos=$_POST['permisos'];
        $num=count($permisos);
        for ($i=0; $i <$num; $i++) { 
          #echo '<script type="text/javascript">alert("'.$permisos[$i].'");</script>';
          mysqli_query($conex,"INSERT INTO usuario_permiso VALUES(0,$iduser,$permisos[$i])");
        }
        header('location:usuarios.php');
      }  
    } 
  } catch (Exception $e) {
    echo 'Excepción capturada: ',  $e->getMessage(), "\n";
  }
}
mysqli_close($conex);
 ?>
