<?php
error_reporting(0);
session_start();
require_once 'conexion.php';
date_default_timezone_set('America/Lima');
$nomb=$_SESSION['nom'];
$valida=mysqli_num_rows(mysqli_query($conex,"SELECT Num_doc FROM usuario where Num_doc='$nomb'"));
if ($nomb== null or $nomb=="" or $valida==0) {
  header("location:../index.html");
}
$moneda=$_POST['moneda'];
if (isset($_POST['f_inicio'])) {
    $f_ini=$_POST['f_inicio'];    
}else{
    $f_ini=date('Y-m-d');
}
if (isset($_POST['f_fin'])) {
    $f_final=$_POST['f_fin'];
}else{
    $f_final=date('Y-m-d');
}
$usuario=mysqli_fetch_row(mysqli_query($conex,"SELECT * FROM usuario WHERE Num_doc='$nomb'"));
$rs_ventas=mysqli_query($conex,"SELECT Fecha,Id_clie,Num_comprob,Moneda,SUM(Total_vta)AS Total FROM venta WHERE Moneda='$moneda' AND Fecha BETWEEN '$f_ini' AND '$f_final' GROUP by Id_clie;");
?>
<!DOCTYPE html>
<html>
<head>
    <title></title>
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="../assets/font-awesome/4.5.0/css/font-awesome.min.css" />
    <!-- text fonts -->
    <link rel="stylesheet" href="../assets/css/fonts.googleapis.com.css" />
    <!-- ace styles -->
    <link rel="stylesheet" href="../assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />
    <link rel="stylesheet" href="../assets/css/ace-skins.min.css" />
    <link rel="stylesheet" href="../assets/css/ace-rtl.min.css" />
    <!-- ace settings handler -->
    <script src="../assets/js/ace-extra.min.js"></script>
</head>
<body>
  <div class="main-content">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> Reporte de Ventas - <?php echo $usuario[1]; ?></h3>
            </div>
            <div class="table-responsive">
                <table id="t_client" class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr align="center">
                            <th>IT</th>
                            <th>Cliente</th>
                            <th>Moneda</th>
                            <th>Total Venta</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php $i=1; $total=0;
                    while ($rpte=mysqli_fetch_row($rs_ventas)) {
                      $cliente=mysqli_fetch_row(mysqli_query($conex,"SELECT * FROM clientes WHERE Id_client=$rpte[1]")); ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><?php echo $cliente[2]; ?></td>
                            <td><?php echo $rpte[3]; ?></td>
                            <td><?php echo $rpte[4]; ?></td>
                        </tr><?php $i++;
                        }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div><!-- /.col -->
  </div>
  
  <script src="../assets/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript">
    if('ontouchstart' in document.documentElement) document.write("<script src='../assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
</script>
<!-- <script src="../assets/js/bootstrap.min.js"></script> -->

<!-- page specific plugin scripts -->
<script src="../assets/js/jquery.dataTables.min.js"></script>
<script src="../assets/js/jquery.dataTables.bootstrap.min.js"></script>
<script src="../assets/js/dataTables.buttons.min.js"></script>
<script src="../assets/js/buttons.flash.min.js"></script>
<script src="../assets/js/buttons.html5.min.js"></script>
<script src="../assets/js/buttons.print.min.js"></script>
<script src="../assets/js/jszip.min.js"></script>
<script src="../assets/js/buttons.colVis.min.js"></script>
<script src="../assets/js/dataTables.select.min.js"></script>

        <!-- ace scripts -->
        <script src="../assets/js/ace-elements.min.js"></script>
        <script src="../assets/js/ace.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#t_client').DataTable({
            "info": false,
            "language": {
            "url": "../assets/js/Spanish.json"
            },
            dom: 'Bfrtip',
            buttons: [
                {"extend": 'print', "text": '<span class = "glyphicon glyphicon-print"> </span>', "className": 'btn btn-success btn-xs'},
                {"extend": 'excel', "text": '<span class = "glyphicon glyphicon-list"></span>', "className": 'btn btn-success btn-xs'}
            ]
        });
    });
</script>
</body>
</html>