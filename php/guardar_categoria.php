<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');
require_once 'conexion.php';
session_start();
$nomb=$_SESSION['nom'];
if ($nomb== null || $nomb=="") { ?>
  <div class="container"><br>
    <div class="alert alert-danger" role="alert"><strong>Atención! </strong>No tiene permiso</div>
  </div><?php
  header("location:index.html");
}
if(isset($_GET['id'])){
  $id_cat=$_GET['id'];
  mysqli_query($conex,"UPDATE categorias SET Estado=0 WHERE Idcat=$id_cat");
  echo '<script language="javascript">alert("Categoria eliminada correctamente");window.location.href="categorias.php";</script>';
}else{
  $fecha=date('Y-m-d');
  $categoria=$_POST['categoria'];
  /*$estado=$_POST['estado'];*/
  $estado=1;
  $actualiza=$_POST['actualiza'];
  $id_cat=$_POST['id_cat'];
  try {
    if ($actualiza==1) {
      mysqli_query($conex,"UPDATE categorias SET categoria='$categoria',fecha='$fecha',estado='$estado' WHERE idcat=$id_cat");
      header('location:categorias.php');
    }else{
      $existe=mysqli_num_rows(mysqli_query($conex,"SELECT * FROM categorias WHERE categoria='$categoria'"));
      /*echo '<script type="text/javascript">alert("'.$existe.'");</script>';*/
      if ($existe>=1) {
      echo '<script>alert("La Categoria ya se encuentra registrada");window.location="categorias.php";</script>';
      }else{
        mysqli_query($conex,"INSERT INTO categorias VALUES(0,'$categoria','$fecha',$estado)");
        header('location:categorias.php');
      }  
    } 
  } catch (Exception $e) {
    echo 'Excepción capturada: ',  $e->getMessage(), "\n";
  }
}
mysqli_close($conex);
?>
