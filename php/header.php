<?php
require_once 'conexion.php';
#session_start();
date_default_timezone_set('America/Lima');
setlocale ( LC_TIME, 'spanish' );
setlocale(LC_ALL,"es_ES@euro","es_ES","esp");
/*$fecha = strftime( "%A %d de %B del %Y" );*/
$fecha = strftime( "%A %d de %B del %Y" );
$nomb=$_SESSION['nom'];
// $valida=mysqli_num_rows(mysqli_query($conex,"SELECT Num_doc FROM usuario where Num_doc='$nomb'"));
// if ($nomb== null or $nomb=="" or $valida==0) {
//  header("location:../index.html");
// }




$usuario=mysqli_fetch_row(mysqli_query($conex,"SELECT * FROM usuario WHERE Num_doc='$nomb'"));
$admin=$usuario[0];
 ?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>Adtech & Service</title>
		<link rel="shortcut icon" href="img/favicon1.ico">
		<meta name="description" content="overview &amp; stats" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="../assets/css/bootstrap.min.css" />
		<link rel="stylesheet" href="../assets/font-awesome/4.5.0/css/font-awesome.min.css" />
		<!-- text fonts -->
		<link rel="stylesheet" href="../assets/css/fonts.googleapis.com.css" />
		<!-- ace styles -->
		<link rel="stylesheet" href="../assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />
		<link rel="stylesheet" href="../assets/css/ace-skins.min.css" />
		<link rel="stylesheet" href="../assets/css/ace-rtl.min.css" />
		    <link rel="stylesheet" href="../assets/css/chosen.min.css" />
		<!-- ace settings handler -->
		<script src="../assets/js/ace-extra.min.js"></script>
		<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->
		<style media="screen">
			input[type="date"]{
				padding: 3px 3px 3px;
				font-size: 12px;
				border-radius: 4px !important;
			}
		</style>
	</head>
	<body class="no-skin">
		<div id="navbar" class="navbar navbar-default ace-save-state">
			<div class="navbar-container ace-save-state" id="navbar-container">
				<button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
					<span class="sr-only">Toggle sidebar</span>

					<span class="icon-bar"></span>

					<span class="icon-bar"></span>

					<span class="icon-bar"></span>
				</button>
				<div class="navbar-header pull-left">
					<a href="#" class="navbar-brand">
						<small>
							<i class="fa fa-plus" style="color: red"></i>
							Adtech & Service - <span style="font-size: 13px"><?php echo ucwords($fecha);?></span>
						</small>
					</a>
				</div>

				<div class="navbar-buttons navbar-header pull-right" role="navigation">
					<ul class="nav ace-nav">
						<li class="light-blue dropdown-modal">
							<a data-toggle="dropdown" href="#" class="dropdown-toggle">
								<img class="nav-user-photo" src="../assets/images/avatars/profile-pic.jpg" alt="Jason's Photo" />
								<span class="user-info">
									<small>Hola</small><?php echo $usuario[1]?>
								</span>

								<i class="ace-icon fa fa-caret-down"></i>
							</a>

							<ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
								<!-- <li>
									<a href="#">
										<i class="ace-icon fa fa-cog"></i>
										Settings
									</a>
								</li> -->

								<li>
									<a href="profile.php">
										<i class="ace-icon fa fa-user"></i>
										Perfil
									</a>
								</li>

								<li class="divider"></li>

								<li>
									<a href="logout.php">
										<i class="ace-icon fa fa-power-off"></i>
										Salir
									</a>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</div><!-- /.navbar-container -->
		</div>

		<div class="main-container ace-save-state" id="main-container">
			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>

			<div id="sidebar" class="sidebar responsive ace-save-state">
				<script type="text/javascript">
					try{ace.settings.loadState('sidebar')}catch(e){}
				</script>
				<ul class="nav nav-list">
					<li class="">
						<a href="escritorio.php">
							<i class="menu-icon fa fa-home"></i>
							<span class="menu-text"> Inicio </span>
						</a>

						<b class="arrow"></b>
					</li>
					<li class="">
						<a href="profile.php">
							<i class="menu-icon brown fa fa-user"></i>
							<span class="menu-text"> Perfil</span>
						</a>
					</li>
					<li class="<?php if (isset($home_menu)){echo "active";}?>">
						<a href="home.php">
							<i class="menu-icon green fa fa-shopping-cart"></i>
							<span class="menu-text"> Ventas</span>
						</a>
					</li>
					<li class="<?php if (isset($producto_menu) || isset($inventario_menu) || isset($categorias_menu) || isset($presentacion_menu) ){echo "active open";}?>">
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon blue fa fa-list"></i>
							<span class="menu-text"> Inventario </span>

							<b class="arrow fa fa-angle-down"></b>
						</a>

						<b class="arrow"></b>

						<ul class="submenu">
							<li class="<?php if (isset($categorias_menu)){echo "active";}?>">
								<a href="categorias.php">
									<i class="menu-icon fa fa-caret-right"></i>
									Categorías
								</a>

								<b class="arrow"></b>
							</li>
							<li class="<?php if (isset($presentacion_menu)){echo "active";}?>">
								<a href="presentacion.php">
									<i class="menu-icon fa fa-caret-right"></i>
									Presentación
								</a>

								<b class="arrow"></b>
							</li>
							<li class="<?php if (isset($producto_menu)){echo "active";}?>">
								<a href="productos.php">
									<i class="menu-icon fa fa-caret-right"></i>
									Productos
								</a>

								<b class="arrow"></b>
							</li>
							
							<li class="<?php if (isset($inventario_menu)){echo "active";}?>">
								<a href="inventario.php">
									<i class="menu-icon fa fa-caret-right"></i>
									Ingreso Inventario
								</a>

								<b class="arrow"></b>
							</li>
						</ul>
					</li>
					<li class="<?php if (isset($clientes_menu)){echo "active";}?>">
						<a href="clientes.php">
							<i class="menu-icon red fa fa-tag"></i>
							<span class="menu-text"> Clientes</span>
						</a>
					</li>
					<li class="<?php if (isset($proveedor_menu) || isset($pagos_menu)){echo "active open";}?>">
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon purple fa fa-truck"></i>
							<span class="menu-text"> Compras </span>
							<b class="arrow fa fa-angle-down"></b>
						</a>
						<b class="arrow"></b>
						<ul class="submenu">
							<li class="<?php if (isset($proveedor_menu)){echo "active";}?>">
								<a href="proveedores.php">
									<i class="menu-icon fa fa-caret-right"></i> Proveedores</a>
								<b class="arrow"></b>
							</li>
							<li class="<?php if (isset($pagos_menu)){echo "active";}?>">
								<a href="compra.php">
									<i class="menu-icon fa fa-caret-right"></i>	Pagos Proveedor</a>
								<b class="arrow"></b>
							</li>
						</ul>
					</li>
					<li class="<?php if (isset($rpte_menu) || isset($det_menu) || isset($deuda_menu) ||isset($rpteinv_menu) || isset($rpteven_menu)){echo "active open";}?>">
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon orange fa fa-pencil-square-o"></i>
							<span class="menu-text"> Reportes </span>
							<b class="arrow fa fa-angle-down"></b>
						</a>
						<b class="arrow"></b>
						<ul class="submenu">
							<li class="<?php if (isset($rpte_menu)){echo "active";}?>">
								<a href="rpte_ventas.php">
									<i class="menu-icon green fa fa-money"></i>
									Ventas
								</a>
								<b class="arrow"></b>
							</li>
      				<li class="<?php if (isset($det_menu)){echo "active";}?>">
								<a href="rpte_detallado.php">
									<i class="menu-icon fa fa-caret-right"></i>
									Detalle Vtas
								</a>
								<b class="arrow"></b>
							</li>
							<li class="<?php if (isset($deuda_menu)){echo "active";}?>">
								<a href="deudas.php">
									<i class="menu-icon green fa fa-credit-card"></i>
									Deudas
								</a>
								<b class="arrow"></b>
							</li>
							<li class="<?php if (isset($rpteinv_menu)){echo "active";}?>">
								<a href="rpte_inventario.php">
									<i class="menu-icon fa fa-caret-right"></i>
									Inventario
								</a>
								<b class="arrow"></b>
							</li>
							<li class="<?php if (isset($rpteven_menu)){echo "active";}?>">
								<a href="rpte_vencidos.php">
									<i class="menu-icon fa fa-caret-right"></i>
									Vencidos
								</a>
								<b class="arrow"></b>
							</li>
							<li class="<?php if (isset($rptetopcliente_menu)){echo "active";}?>">
								<a href="rpte_top_clientes.php">
									<i class="menu-icon fa fa-caret-right"></i>
									Top Clientes
								</a>
								<b class="arrow"></b>
							</li>
						</ul>
					</li>
					<li class="<?php if (isset($usuario_menu) || isset($tipo_doc_menu) || isset($empresa_menu)){echo "active open";}?>">
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon orange fa fa-cogs"></i>
							<span class="menu-text"> Acceso </span>
							<b class="arrow fa fa-angle-down"></b>
						</a>
						<b class="arrow"></b>
						<ul class="submenu">
							<li class="<?php if (isset($empresa_menu)){echo "active";}?>">
								<a href="empresa.php"><i class="menu-icon fa fa-bank"></i>	Empresa</a><b class="arrow"></b>
							</li>
							<li class="<?php if (isset($usuario_menu)){echo "active";}?>">
								<a href="usuarios.php"><i class="menu-icon fa fa-user"></i> Usuario</a><b class="arrow"></b>
							</li>
							<li class="<?php if (isset($tipo_doc_menu)){echo "active";}?>">
								<a href="tipo_documento.php"><i class="menu-icon fa fa-caret-right"></i> Documentos</a><b class="arrow"></b>
							</li>
						</ul>
					</li>
				</ul><!-- /.nav-list -->

				<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
					<i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
				</div>
			</div>

			<div class="main-content">	
	</body>
</html>
